from utils       import *
from global_map  import *

NUMERIC_POSTCONDITIONS = {
    
}


def build_process_graph(g_p):
    nps = []
    G = DiGraph()
    indx_np = 0
    for p_indx, p_pre, p_eff in g_p:
        for np in p_eff[RVALUE]:

            affected_variable = np[RVALUE][LVALUE]

            pre_leaves = set()
            extract_leaves(p_pre, pre_leaves)
            pre_leaves = set([x for x in pre_leaves if is_function(x)])
            eff_leaves = set()
            extract_leaves(np[RVALUE][RVALUE], eff_leaves)
            eff_leaves = set([x for x in eff_leaves if is_function(x)])
            nps.append((indx_np, set([affected_variable]), pre_leaves, eff_leaves))
            G.add_node(indx_np)
            indx_np += 1

    for indx_1, av_1, pre_1, eff_1 in nps:
        for indx_2, av_2, pre_2, eff_2 in nps:
            if indx_1 != indx_2:

                if len(eff_1 & pre_2) > 0:
                    G.add_edge(indx_1, indx_2)

                if len(av_1 & eff_2) > 0:
                    G.add_edge(indx_2, indx_1)

                if len(av_1 & pre_2) > 0:
                	G.add_edge(indx_2, indx_1)
    
    print ('draw')
    draw(G, with_labels=True)
    plt.show()


def build_g_nps(g_p):
    a_vars = set()  # a_vars stands for affected_variables

    for indx_p, pre_p, eff_p in g_p:
        nps = eff_p[RVALUE]

        for np in nps:
            op   = np[LVALUE]
            expr = np[RVALUE]
            x = expr[LVALUE]
            if x not in a_vars:
                a_vars.add(x)
            else:
                print ('They exist at least two expressions having the same lvalues')
                return COPYALL
    print ('No same lvalues! Use: INVALID')
    return INVALID
