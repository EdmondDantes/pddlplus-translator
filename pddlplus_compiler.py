from utils        import *
from parser       import *
from grounder     import *
from printer      import *
from compiler     import *


@click.command()
@click.option('--domain_path',          required=True,  type=click.Path(exists=True), help = 'Fullpath of the PDDL+ domain.')
@click.option('--problem_path',         required=True,  type=click.Path(exists=True), help = 'Fullpath of the PDDL+ problem.')
@click.option('--output_path',          required=True,  type=click.Path(exists=True), help = 'Path in which put the compiled problem.')
@click.option('--schema',               required=False, type=str,                     help = 'Schema has to be in {EXP, POLY, EXPL, POLY-}')
@click.option('--delta',                required=False, type=str,                     help = 'Time discretization step',              default=1)
@click.option('--no_event_opt_cascade', required=True,  type=bool,                    help = 'Disable event optimization [Cascade]',  default=False)
@click.option('--no_event_opt_action',  required=True,  type=bool,                    help = 'Disable event optimization [Action]',   default=False)
@click.option('--plan',                 required=False, type=str,                     help = '')


def main(domain_path, problem_path, output_path, schema, delta, no_event_opt_cascade, no_event_opt_action, plan):
    
    check_input(schema, remap(schema), delta, plan)

    schema = remap(schema)

    print ('Domain path           : {}'.format(domain_path))
    print ('problem_path          : {}'.format(problem_path))
    print ('Schema                : {}'.format(schema))
    print ('Delta                 : {}'.format(delta))
    print ('Event Opt [Cascade]   : {}'.format('Not Enabled' if no_event_opt_cascade is True else 'Enabled'))
    print ('Event Opt [Action]    : {}'.format('Not Enabled' if no_event_opt_action is True else 'Enabled'))
    if plan is not UNDEFINED_PAR:
        print ('Plan                  : {}'.format(plan))
        print ('\nVALIDATING the following plan:')
        plan_str = readfile(plan)
        print (plan_str)

    mode = TRANSL_per_VAL if plan is not UNDEFINED_PAR else TRANSLATION

    print_header('PARSING')
    domain_dict  = parse_domain(domain_path)

    problem_dict = parse_problem(problem_path, domain_dict[TYPES])
    
    if plan is not UNDEFINED_PAR:
        plan_data, makespan  = parse_plan(plan)
        val_schema = schema.split('+')[0]
        schema     = schema.split('+')[1]
    else:
        plan_data  = NO_PLAN
        makespan   = None
        val_schema = None

    print_header ('GROUNDING')
    g_problem_dict = ground_problem(domain_dict, problem_dict)
    g_domain_dict  = ground_domain(domain_dict, problem_dict, g_problem_dict[INIT_STATE])


    print_header('COMPILING PLAN INTO DOMAIN')

    normalize_goal(g_problem_dict)

    ''' code for producing a validating task '''
    if plan_data is not NO_PLAN:
        if schema in [COPYALL, EXP, COPYALLplus]:
            g_domain_dict  = compile_plan_into_domain(g_domain_dict, plan_data, makespan, mode, delta, schema, val_schema)
            g_problem_dict = compile_plan_into_problem(g_problem_dict, plan_data, makespan, mode, delta, schema, val_schema)
        elif schema == 'plain':
            g_domain_dict  = compile_plan_into_domain(g_domain_dict, plan_data, makespan, mode, delta, schema, val_schema)
            g_problem_dict = compile_plan_into_problem(g_problem_dict, plan_data, makespan, mode, delta, schema, val_schema)

    # grounded_domain_str = print_grounded_domain(g_domain_dict_pddlplus)
    # g_domain_output     = join(output_path, 'grounded_{}'.format(get_last_name(domain_path)))
    # writefile(g_domain_output, grounded_domain_str)
    # exit(1)

    translate_and_save(
        g_domain_dict, g_problem_dict, 
        schema, val_schema, delta, 
        no_event_opt_cascade,  no_event_opt_action,
        domain_path, problem_path, output_path, plan_data, makespan)

    # if plan is NO_PLAN or val_schema in [COPYALL, EXP] or val_schema is None:
    # elif val_schema == 'plain':
    #     
    #     grounded_domain_str = print_grounded_domain(g_domain_dict)
    #     '''
    #     to refactor, merge with save in translate_and_save procedure
    #     '''
    #     g_domain_output     = join(output_path, 'compiled_{}'.format(get_last_name(domain_path)))
    #     print (grounded_domain_str)
    #     exit(1)
    #     writefile(g_domain_output, grounded_domain_str)
    #     grounded_problem_str = print_grounded_problem(g_problem_dict_pddplus, g_domain_dict)
    #     g_problem_output     = join(output_path, 'compiled_{}'.format(get_last_name(problem_path)))
    #     writefile(g_problem_output, grounded_problem_str)

    # if plan is not NO_PLAN:
    #     translate_and_save(
    #         g_domain_dict_pddlplus, g_problem_dict_pddplus, 
    #         schema, delta, 
    #         no_event_opt_cascade, no_event_opt_action,
    #         domain_path, problem_path, output_path, PDDPLUS_OUTPUT)        


def translate_and_save(g_domain_dict, g_problem_dict, schema, val_schema, delta, no_event_opt_cascade, no_event_opt_action, domain_path, problem_path, output_path, plan_data, makespan):

    prefix = 'compiled_{}'
    print ('COMPILING' + (' - from PDDL+ to PDDL2.1' if val_schema != 'plain' else ' - from PDDL+ to PDDL+'))
    print ('Compiling domain')
    compiled_domain = compile_domain(
        g_domain_dict, 
        schema, val_schema, 
        no_event_opt_cascade, 
        no_event_opt_action,
        plan_data, makespan)

    print ('Compile problem')
    compiled_problem = compile_problem(
        g_problem_dict, 
        compiled_domain[G_FUNCT], 
        g_domain_dict[G_E], schema, val_schema, delta)
    print ('\n')

    print ('Saving compiled planning task')
    compiled_domain_str = print_grounded_domain(compiled_domain)
    comp_dom_path = join(output_path, prefix.format(get_last_name(domain_path)))
    writefile(comp_dom_path, compiled_domain_str)

    comp_prob_path = join(output_path, prefix.format(get_last_name(problem_path)))
    compiled_problem_str = print_grounded_problem(compiled_problem, compiled_domain)
    writefile(comp_prob_path, compiled_problem_str)


def normalize_goal(g_problem_dict):
    ''' compiling the goal (make function!) '''
    goal_state = g_problem_dict[GOAL_STATE]
    # if there is just a predicate in the goal (refactor-> sistema da un'altra parte)
    if type(goal_state) == int:
        lvalue = AND
        rvalue = [goal_state]
        g_problem_dict[GOAL_STATE] = lvalue, rvalue
    return g_problem_dict


def check_input(old_schema, schema, delta, plan):
    print_header('CHECKING INPUT')

    # check delta
    try:
        float(delta)
    except ValueError:
        print ('Delta = {}, Not a float'.format(delta))
        exit(1)
    if not float(delta) > 0:
        print ('Delta = {}, has to be > 0'.format(delta))
        exit(1)

    if plan is NO_PLAN:
        if schema not in ALLOWED_SCHEME:
            print ('Schema = {}, has to be in {}'.format(old_schema, set(ALLOWED_SCHEME_EXTERNAL)))
            exit(1)
    else:
        if schema not in COMPOSED_SCHEME:
            print ('If a plan is specified then Schema = {} has to specify a validation translation.'.format(old_schema))
            print ('\t{}   : {}  [from PDDL+ to PDDL+], {}  [from PDDL+ to PDDL2.1]'.format(Z0p, Z0_PLAINp, Z0_POLYp))
            print ('\t{}   : {}  [from PDDL+ to PDDL+], {}  [from PDDL+ to PDDL2.1]'.format(Z1p, Z1_PLAINp, Z1_POLYp))
            print ('\t{}  : {} [from PDDL+ to PDDL+], {} [from PDDL+ to PDDL2.1]'.format(Z2p, Z2_PLAINp, Z2_POLYp, Z2_POLYVp))
            print ('\tPOLYV')
            exit(1)

    # check schema

    # if schema in [Z0, Z0_COPYALL, Z0_EXP] and plan is NO_PLAN:
    #     print ('If a plan is not specified then Schema = {} has to be in {}'.format(
    #         schema, set([COPYALL, EXP, INVALID])))
    #     exit(1)
    # elif schema in [COPYALL, EXP, INVALID] and plan is not NO_PLAN:
    #     print ('If a plan is specified then Schema = {} has to be in {}'.format(
    #         schema, set([Z0, Z0_COPYALL, Z0_EXP])))
    #     exit(1)

    print ('Passed\n')



def remap(schema):
    if schema == POLY:
        return COPYALL
    elif schema == EXPbold:
        return EXP
    elif schema == POLYMINUS:
        return INVALID
    elif schema == EXPLOCAL:
        return INVALIDplus
    elif schema == Z0_PLAINp:
        return Z0_PLAIN
    elif schema == Z0_POLYp:
        return Z0_POLY
    elif schema == Z1_PLAINp:
        return Z1_POLY
    elif schema == Z1_POLYp:
        return Z1_POLY
    elif schema == Z2_PLAINp:
        return Z2_PLAIN
    elif schema == Z2_PLAINp:
        return Z2_PLAIN
    elif schema == Z2_POLYp:
        return Z2_POLY
    elif schema == Z2_POLYVp:
        return Z2_COPYALL_2
    return schema


if __name__ == "__main__":
    main()