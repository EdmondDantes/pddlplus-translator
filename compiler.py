from utils                  import *
from global_map             import *
from numeric_postconditions import *
from val_compiler           import * # necessary for ad-hoc copyall

from sympy.abc                  import x
from sympy                      import Poly
from sympy.solvers.inequalities import solve_rational_inequalities


def compile_domain(g_domain_dict, comp_scheme, val_schema, no_event_opt_cascade, no_event_opt_action, plan_data, makespan):
    '''
    Main function which compile a PDDL+ domain 
    '''
    g_pred  = g_domain_dict[G_PRED]
    g_funct = g_domain_dict[G_FUNCT]
    g_e     = g_domain_dict[G_E]
    g_a     = g_domain_dict[G_A]
    g_p     = g_domain_dict[G_P]

    # build_process_graph(g_p)

    # MAKE FUNCTION
    invalid_block_same_lvalues = False # Aggiungi controllo per processi che predicano su stesso lvalues
    schema2use = build_g_nps(g_p)
    if comp_scheme == INVALID and schema2use == COPYALL:
        invalid_block_same_lvalues = True
    if comp_scheme == POLYBEST:
        comp_scheme = schema2use
    # END-MAKE FUNCTION

    '''
    ADD new propositional variables
    '''
    g_pred_prime  = add_new_predicates(g_pred, g_e, g_p, comp_scheme)

    '''
    ADD new numeric variables
    '''
    g_funct_prime = add_new_functions(g_funct, comp_scheme)


    '''
    MODIFY the native actions to making them compatible with the translations
    '''
    g_a_prime     = compiled_grounded_action(g_a, g_e, comp_scheme, no_event_opt_action)


    ''' REFACTOR THIS UGLY PART '''
    if comp_scheme != 'plain':

        if check_exists_event(g_e):
            g_a_prime.append(generate_simulate_events_action(g_e, no_event_opt_cascade))

        g_a_prime = compile_processes(
            g_p, g_e, comp_scheme, g_a_prime, g_funct, 
            invalid_block_same_lvalues, plan_data, makespan)

        compiled_domain_dict = {
            DOMAIN_NAME : g_domain_dict[DOMAIN_NAME],
            G_A         : g_a_prime,
            G_E         : [],
            G_P         : [],
            G_PRED      : g_pred_prime,
            G_FUNCT     : g_funct_prime
        }
    elif comp_scheme == 'plain':
        compiled_domain_dict = {
            DOMAIN_NAME : g_domain_dict[DOMAIN_NAME],
            G_A         : g_a_prime,
            G_E         : g_domain_dict[G_E],
            G_P         : g_domain_dict[G_P],
            G_PRED      : g_pred_prime,
            G_FUNCT     : g_funct_prime
        }

    return compiled_domain_dict


def add_new_predicates(g_pred, g_e, g_p, comp_scheme):
    '''
    Function which adds to the original predicate set some
    novel predicates.
    '''
    if comp_scheme in [INVALID, INVALIDplus, EXP]:  # fai caso esponenziale separato [alcuni predicati non servono]
        g_pred = add_new_predicates_invalid(g_pred, g_e)

    elif comp_scheme in [COPYALL, COPYALLplus]:
        g_pred = add_new_predicates_copyall(g_pred, g_e, g_p)

    elif comp_scheme == EXP:
        pass


    return g_pred


def add_new_predicates_copyall(g_pred, g_e, g_p):
    '''
    Function that adds novel predicates for ''invalid'' compilation 
    scheme
    '''

    # Adding events predicates
    if check_exists_event(g_e): 
        # Adding force-events.
        g_pred.add(generate_new_predicate(FORCE_EVENTS))
        # Adding fired predicates.
        [g_pred.add(generate_new_predicate(get_fired_pred(e_indx))) for (e_indx, _, _) in g_e]
     
    g_pred.add(generate_new_predicate(PAUSE))

    ######################
    # REFACTOR THIS PART #
    ######################
    # Adding done predicates
    indx_done = 0    
    for (p_indx, _, p_eff) in g_p:

        assert p_eff[LVALUE] == AND
        list_of_nps = p_eff[RVALUE]
        for i in range(0, len(list_of_nps)):
            done_pred = generate_new_predicate(get_done_pred(indx_done))
            g_pred.add(done_pred)
            add_done_pred_global(indx_done, done_pred)
            indx_done += 1

    done_pred = generate_new_predicate(get_done_pred(indx_done))
    g_pred.add(done_pred)
    add_done_pred_global(indx_done, done_pred)
    return g_pred


def add_new_predicates_invalid(g_pred, g_e):
    '''
    Function that adds novel predicates for ''copyall'' compilation 
    scheme
    '''
    if check_exists_event(g_e): 
        # Adding force-events.
        g_pred.add(generate_new_predicate(FORCE_EVENTS))
        # Adding fired predicates.
        for (e_indx, _, _) in g_e:
            g_pred.add(generate_new_predicate(get_fired_pred(e_indx)))
    g_pred.add(generate_new_predicate(PAUSE))
    g_pred.add(generate_new_predicate(UNDEFINED))
    return g_pred


def add_new_functions(g_funct, comp_scheme):
    g_funct.add(generate_new_function(DELTA))

    if comp_scheme == INVALID:
        pass

    elif comp_scheme in [COPYALL, COPYALLplus]:
        g_funct = add_new_functions_copyall(g_funct)
    elif comp_scheme in [EXP, INVALIDplus]:
        g_funct.add(generate_new_function(MINUS_DELTA)) 

    return g_funct


def add_new_functions_copyall(g_funct):
    g_funct_copy = set()
    for f_indx in g_funct:
        copy_indx = generate_new_function(get_copy_function(f_indx))
        g_funct_copy.add(copy_indx)
        update_copy_indx_dict(f_indx, copy_indx)
    return g_funct | g_funct_copy


def get_copy_function(f_indx):
    return COPY.format(get_predicate_name(f_indx))


def check_exists_event(g_e):
    return len(g_e) > 0


def compiled_grounded_action(g_a, g_e, comp_scheme, no_event_opt_action):
    ''' 
    Action Compilation
    '''
    pause_indx = get_predicate_indx(PAUSE)         # refactor name! refactor position!
    fe_actions = 0
    if comp_scheme in [INVALID, COPYALL, COPYALLplus, EXP]:
        for indx_a, (a_name, pre, eff) in enumerate(g_a, start=0):
            '''
            DO SOMETHING FOR HANDLING EVENTS
            '''
            if check_exists_event(g_e):
                force_events_indx = get_predicate_indx(FORCE_EVENTS)  # refactor name! refactor position!
 
                # event optimization for actions
                if not check_neutral_event_operator(eff, g_e, no_event_opt_action):  
                    eff = add_pred_to_formula(eff, force_events_indx, POS)
                    fe_actions += 1
                pre = add_pred_to_formula(pre, force_events_indx, NEG)

            if comp_scheme in [COPYALL, COPYALLplus]:
                pre = add_pred_to_formula(pre, pause_indx, NEG)
            g_a[indx_a]  = a_name, pre, eff
    try: 
        ratio_fe = round(fe_actions / len(g_a), 2)
        print ('\nEVENT OPTIMIZATIONS\nForced Events action: {}'.format(ratio_fe))
    except:
        # Caso limite in cui non ci sono azioni, ma solo eventi.
        pass

    return g_a


def check_neutral_event_operator(eff, g_e, no_event_opt_action):
    '''
    Function that, given an action a and E (set of all the events),
    return if a can trigger at-least one event.
    '''
    if no_event_opt_action is True:  # INPUT PARAMETER
        return False

    op_eff = set()
    extract_leaves(eff, op_eff)

    for indx_e, pre_e, eff_e in g_e:
        ev_pre = set()
        extract_leaves(pre_e, ev_pre)
        if (len(ev_pre.intersection(op_eff)) > 0):
            return False
    return True


def add_pred_to_formula(formula, pred_indx, positive):
    '''
    Functions whis add the predicate force-events to a formula
    in positive or negative way.
    '''
    if type(formula) == int: # force AND when there is just a predicate; e.g.: :effects (pred)
        formula = AND, formula

    lvalue = formula[LVALUE]
    rvalue = formula[RVALUE]

    if positive is POS:
        to_add = pred_indx
    else:
        to_add = NOT, pred_indx

    if lvalue == AND:
        assert type(rvalue) == list
        rvalue.append(to_add)
        return lvalue, rvalue
    else:
        return AND, [rvalue, to_add]       


def generate_simulate_events_action(g_e, no_event_opt_cascade):
    global baxter_cascading
    '''
    Function with generates the simulta-events action. This
    action contains a set of conditional effects:
        - fired    :
        - saturated:
        - trigger  : to catch the events precondition
    '''
    
    if no_event_opt_cascade is False:
        cascading = check_cascading(g_e)
    else:
        cascading = True 

    ''' to check: why the same values ??? '''
    #force_events_indx = get_action_indx(FORCE_EVENTS)
    #print (force_events_indx)
    force_events_indx = get_predicate_indx(FORCE_EVENTS)
    #print (force_events_indx)

    pre  = AND, force_events_indx

    when_list = []
    if cascading:
        print ('Cascading events\n')
        when_fired     = generate_when_fired(g_e)
        when_saturated = generate_when_saturated(g_e)
        when_list.append(when_fired)
        when_list.append(when_saturated)
    else:
        print ('No cascading events\n')
        when_list.append((NOT, get_predicate_indx(FORCE_EVENTS)))

    when_trigger = generate_when_trigger(g_e)
    when_list.append(when_trigger)

    eff = AND, when_list
    indx_action = update_grounded_action(SIMULATE_EVENTS, [])
    return (indx_action, pre, eff)


def check_cascading(g_e):

    ''' Refactor this bad function '''
    if len(g_e) <= 1:
        return NO_CASCADING_E
    event_dict = {}
    for e_indx1, e_pre1, e_eff1 in g_e:
        for e_indx2, e_pre2, e_eff2 in g_e:
            if e_indx1 != e_indx2:
                eff1_leaves = set()
                extract_leaves(e_eff1, eff1_leaves)
                pre2_leaves = set()
                extract_leaves(e_pre2, pre2_leaves)
                interferences = eff1_leaves.intersection(pre2_leaves)
                if len(interferences) > 0:
                    if check_indepth_event(e_pre1, e_eff1, e_pre2, e_eff2, interferences, e_indx1, e_indx2):
                        
                        # print (get_something_name(e_indx1, None))
                        # print (get_something_name(e_indx2, None))
                        return YES_CASCADING_E
    return NO_CASCADING_E


def get_numeric_prop_expr(expr_eff1):
    num  = []
    prop = []
    for e in expr_eff1:

        if type(e) == int:
            prop.append(e)
        elif type(e) == tuple:
            op, expr = e
            if op in [INCREASE, DECREASE, ASSIGN, LESS, LESSEQ, GREATER, GREATEREQ]:
                num.append(e)
            elif op in [EQUAL]:
                ''' TODO: da qualche parte tieni traccia di cosa e' numerico e cosa e' proposizionale '''
                num.append(e)
            elif op in [NOT]:
                prop.append(e)

            else:
                print ('extend \'get_numeric_prop_expr\' [in compiler.py]')
                print (e)
                exit(1)

    return num, prop


def check_indepth_event(e_pre1, e_eff1, e_pre2, e_eff2, interferences, e1, e2):
    op1, expr_eff1 = e_eff1
    op2, expr_pre2 = e_pre2
    if op1 != AND or op2 != AND:
        return False

    trigger_dict = {}

    op, expr_pre1 = e_pre1

    num_expr1_eff1, prop_expr1_eff1 = get_numeric_prop_expr(expr_eff1)
    num_expr2_pre2, prop_expr2_pre2 = get_numeric_prop_expr(expr_pre2)

    ''' numeric triggering '''
    for lvalue1, rvalue1 in num_expr1_eff1:
        for lvalue2, rvalue2 in num_expr2_pre2:
            var1, value1 = rvalue1
            var2, value2 = rvalue2

            if var1 == var2 and var1 in interferences:

                if lvalue1 == ASSIGN:
                    assert lvalue1 == ASSIGN
                    trigger = evaluate_interference(value1, lvalue2, value2)
                    if trigger is False:
                        return NO_CASCADING_E

                elif lvalue1 in [INCREASE, DECREASE]:
                    trigger = evaluate_add_interference(expr_pre1, lvalue1, rvalue1, lvalue2, rvalue2, var1)
                    if trigger is False:
                        return NO_CASCADING_E
                else:
                    print ('Extend Code in  function \'check_indepth_event\'')
                    print (lvalue1, rvalue1)
                    print (lvalue2, rvalue2)
                    exit(1)
                # print (trigger)
                # assert var1 not in trigger_dict.keys()
                # trigger_dict[var1] = trigger

    ''' propositional triggering '''
    for p1 in prop_expr1_eff1:
        for p2 in prop_expr2_pre2:

            ''' TODO: check false relation ship '''
            if p1 == p2 and extract_variable(p2) in interferences:
                continue
                # trigger = True
                # assert p1 not in trigger_dict.keys()
                # trigger_dict[p1] = True
            elif p1 != p2 and extract_variable(p1) in interferences and extract_variable(p2) in interferences:
                return NO_CASCADING_E

    # for x, y in trigger_dict.items():
    #     if y is False:
    #         return NO_CASCADING_E

    return YES_CASCADING_E


def extract_variable(p):
    # print ('TEST')
    # print (p)
    if (type(p) == int):
        return p
    else:
        rvalue = p[1]
        assert p[0] == NOT
        assert len(rvalue) == 1
        p = rvalue[0]
        if (type(p) == int):
            return p


def get_pre_var(var, e_pre):
    for e in e_pre:
        symb, rvalue = e
        var_e, expr  = rvalue
        if var == var_e:
            return e
    ''' fix this instruction '''
    print ('Extend Code in  function \'get_pre_var\'')
    exit(1)


def evaluate_add_interference(expr_pre1, lvalue1, rvalue1, lvalue2, rvalue2, var):
    '''
    '''
    # get precondition affecting var from the attacker action or event
    lvalue, rvalue = get_pre_var(var, expr_pre1)
    inequality1    = build_inequality(lvalue2, rvalue2, lvalue1, rvalue1)
    inequality2    = build_inequality(lvalue, rvalue, None, None)

    TEMPLATE_INEQ_SOLVER = 'solve_rational_inequalities([[#INEQ1#, #INEQ2#]])'

    ineq_command = TEMPLATE_INEQ_SOLVER.replace(
        '#INEQ1#', inequality1).replace('#INEQ2#', inequality2)

    '''
    empty set == insieme delle soluzioni vuoto
    ''' 
    # print (ineq_command)
    # print (eval(ineq_command))
    return (str(eval(ineq_command)) != 'EmptySet')


def build_inequality(lvalue, rvalue, add_lvalue, add_rvalue):
    POLY_TEMPLATE = '((Poly(#EXPR#), Poly(1, x)), \'#RELATIONSHIP#\')'
    var, expr = rvalue
    ''' 
    OUTPUT EXAMPLE 
    ((Poly(x + 4), Poly(1, x)), '<')
    '''
    poly_arg = '#VAR# - (' + str(eval_tup(expr))

    var_str = '(x'

    if add_lvalue is not None:
        if add_lvalue == INCREASE:
            var_str += ' + '
        elif add_lvalue == DECREASE:
            var_str += ' - '
        else:
        	print ('Extend \'build_inequality\'')
        	exit(1)
        var, add_expr = add_rvalue
        var_str += add_expr
    var_str  += ')'
    poly_arg += ')'
    poly_arg = poly_arg.replace('#VAR#', var_str)

    inequality = POLY_TEMPLATE.replace('#EXPR#', poly_arg).replace('#RELATIONSHIP#', lvalue)

    return inequality


def evaluate_interference(value1, lvalue2, value2):
    str2compare = ''
    str2compare += eval_tup(value1)
    str2compare += eval_tup(lvalue2)
    str2compare += eval_tup(value2)
    ''' stampa stringa da valutare '''
    return eval(str2compare)


def eval_tup(tup):
    if type(tup) != tuple:
        if str(tup) == '=':
            return '=='
        else:
            return str(tup)
    else:
        lvalue, rvalue = tup
        operator = lvalue
        operand1, operand2 = rvalue[0], rvalue[1]
        return '(' + eval_tup(operand1) + operator + eval_tup(operand2) + ')'


def generate_when_fired(g_e):
    '''
    This functions generates the conditional effects which make true
    the fired predicate for each grounded event
    '''
    when_list = []
    [when_list.append((WHEN, [pre_e, get_predicate_indx(get_fired_pred(indx_e))])) for (indx_e, pre_e, _) in g_e]
    return when_list


def generate_when_saturated(g_e):
    # force_events_indx = get_action_indx(FORCE_EVENTS)
    force_events_indx = get_predicate_indx(FORCE_EVENTS)

    and_condition = []
    reset_fired = []
    for (indx_e, pre_e, _) in g_e:
        fired_e = OR, [(NOT, pre_e), get_predicate_indx(get_fired_pred(indx_e))]

        and_condition.append(fired_e)
        reset_fired.append((NOT, get_predicate_indx(get_fired_pred(indx_e))))
    reset_effects = AND, [(NOT, force_events_indx)] + reset_fired
    return [(WHEN, [(AND, and_condition), reset_effects])]


def generate_when_trigger(g_e):
    when_list = []

    for (indx_e, pre_e, post_e) in g_e:
        when_list.append((WHEN, [pre_e, post_e]))
    return when_list


'''
SIMULATE-PROCESSES
'''
def compile_processes(g_p, g_e, comp_scheme, g_a_prime, g_funct, invalid_block_same_lvalues, plan_data, makespan):

    if comp_scheme == INVALID:
        g_a_prime.append(generate_simulate_processes(g_p, g_e, invalid_block_same_lvalues))

    elif comp_scheme in [COPYALL, COPYALLplus]:
        g_a_prime = g_a_prime + generate_copyall_simulate_processes_actions(g_p, g_e, g_funct, comp_scheme, plan_data, makespan)

    elif comp_scheme == EXP:
        g_a_prime = g_a_prime + generate_exp_simulate_processes_actions(g_p, g_e, g_funct)

    elif comp_scheme == INVALIDplus:
        g_a_prime.append(generate_localexp_simulate_processes_actions(g_p, g_e, g_funct))

    return g_a_prime


def generate_localexp_simulate_processes_actions(g_p, g_e, g_funct):
    empty_prec_proc = get_process_empty_prec(g_p)

    affected_dict = {}
    for indx_p, p in enumerate(g_p, start=0):
        id_p, pre_p, eff_p = p
        log_expr, eff_list = eff_p
        assert log_expr == AND

        for eff in eff_list:
            op, rvalue = eff # _ == increase or decrease
            var = rvalue[LVALUE]
            if var not in affected_dict.keys():
                affected_dict[var] = {}
            if indx_p not in affected_dict[var].keys():
                affected_dict[var][indx_p] = []
            affected_dict[var][indx_p].append(eff)

    ''' log '''
    ntot = 0
    nmax = -1
    for x, xx in affected_dict.items():
        ntot += len(xx)
        nmax = max(nmax, len(xx))
    print ('STATS')
    print ('Ntot: {}'.format(ntot))
    print ('Nmax: {}\n'.format(nmax))


    when_list = []
    for x_var, affecting_ps in affected_dict.items():

        if len(affecting_ps) == 0:
            pass
        else:
            when_list += generate_simulate_when_for_x(g_p, x_var, affecting_ps)

    when_list.append((INCREASE, [TOTAL_TIME, get_predicate_indx(DELTA)]))

    exists_e = check_exists_event(g_e)
    if exists_e:
        # force_events_indx = get_action_indx(FORCE_EVENTS)
        force_events_indx = get_predicate_indx(FORCE_EVENTS)

    indx_action = update_grounded_action(SIMULATE_PROCESSES, [])

    if exists_e:
        pre  = AND, (NOT, force_events_indx)
    else:
        pre  = AND, PARSED_EMPTY_PRECONDITION
    eff_list = when_list
    if exists_e: 
        eff_list = eff_list + [force_events_indx]
    eff = AND, eff_list
    return (indx_action, pre, eff)


def generate_simulate_when_for_x(g_p, x_var, affecting_ps):
    '''
    Function which build the SIMULATE action for EXPlocal (invalid-complete)
    '''
    when_x = []

    b_strings = generate_binary(len(affecting_ps))[1:] # Excluding the first 000...0 string. 

    ps_indxs = list(affecting_ps.keys())
    ps_indxs.sort()


    for context in b_strings:
        w_cond_list = []
        w_eff_list  = []
        '''
        Building precondition
        '''
        for indx_b, bi in enumerate(context):
            _, pre_p, _ = g_p[ps_indxs[indx_b]] # Recupero le precondizioni
            and_expr, pre_list = pre_p

            if pre_p == PARSED_EMPTY_PRECONDITION:
                w_eff_list += affecting_ps[ps_indxs[indx_b]]
            else:

                if len(pre_list) == 1:
                    pre_p = pre_list

                assert and_expr == AND    
                if bi == '0':
                    w_cond_list.append((NOT, pre_p))
                elif bi == '1': # true
                    w_cond_list.append(pre_p)
                    w_eff_list += affecting_ps[ps_indxs[indx_b]] # Aggiungo gli effetti che influenzano x_var

        w_cond_pre = AND, w_cond_list

        # w_cond_eff = AND, w_eff_list

        w_cond_eff = AND, merge_invalidplus_eff_list(w_eff_list)

        when_x.append((WHEN, [w_cond_pre, w_cond_eff]))

    return when_x


def merge_invalidplus_eff_list(w_eff_list):
    '''
    INVALIDplus
    '''
    test_x_values = set()
    to_merge = []
    for op, rl in w_eff_list:
        lvalue, rvalue = rl
        test_x_values.add(lvalue)
        to_merge.append((op, rvalue))
    assert len(test_x_values) == 1  # control
    if len (w_eff_list) == 1:
        tail = []
    else:
        tail = to_merge[1:]
    return INCREASE, [test_x_values.pop(), generate_recursive_expression(to_merge[0], to_merge[1:])]


def generate_exp_simulate_processes_actions(g_p, g_e, g_funct):
    empty_prec_proc = get_process_empty_prec(g_p)
    sub_g_p = [p for p in g_p if p not in empty_prec_proc]
    n = len(sub_g_p)
    b_string = generate_binary(n)
    actions_list = []
    for i in b_string:
        pre_list = []
        eff_list = []

        name        = 'simulate_processes_{}'.format(i)
        indx_action = update_grounded_action(name, [])

        for indx_p, g_p_indx in enumerate(sub_g_p): 
            _, pre_g_p_indx, eff_g_p_indx = g_p_indx
            tf = i[indx_p]
            if tf == '0': # false
                pre_list.append((NOT, pre_g_p_indx))
            else: # true
                pre_list.append(pre_g_p_indx)
                eff_list.append(eff_g_p_indx)

        for indx_p, g_p_indx in enumerate(empty_prec_proc):
            _, pre_g_p_indx, eff_g_p_indx = g_p_indx
            eff_list.append(eff_g_p_indx)

        merged_exprs_list = merge_same_lvalues(eff_list)
        merged_exprs_list.append((INCREASE, [TOTAL_TIME, get_predicate_indx(DELTA)]))
        if check_exists_event(g_e): # refactor: fallo una volta...
            merged_exprs_list.append(get_predicate_indx(FORCE_EVENTS))
            pre_list.append((NOT, get_predicate_indx(FORCE_EVENTS)))

        pre = AND, pre_list
        eff = AND, merged_exprs_list
        actions_list.append((indx_action, pre, eff))
    return actions_list


def get_process_empty_prec(g_p):
    '''
    This function check which processes have empty precondition
    '''
    empty_prec = []
    for p in g_p:
        _, pre_p, _ = p
        if pre_p == PARSED_EMPTY_PRECONDITION:
            empty_prec.append(p)
    return empty_prec


def merge_same_lvalues(eff_list):
    if eff_list == []:
        return eff_list

    # costruisco dizionario effetti che predicano su medesima variabile
    aff_vars_dict = {}
    for e in eff_list:
        lvalue, nps_list = e
        assert lvalue == AND
        for np in nps_list:

            operator, expr = np
            assert operator in [INCREASE, DECREASE]
            affected_variable, expr = expr
            if affected_variable not in aff_vars_dict.keys():
                aff_vars_dict[affected_variable] = []
            aff_vars_dict[affected_variable].append((operator, expr))
    

    # genero espressioni compilate sommando i diversi contributi
    merged_exprs_list = []
    for indx_var, expr_list in aff_vars_dict.items():
        
        if len(expr_list) == 1:
            operator, expr = expr_list[0]
            merged_exprs_list.append((operator, [indx_var, compile_exp_expr(expr, INCREASE)])) # INCREASE --> delta positivo
        else:
            x = (INCREASE, [indx_var, generate_recursive_expression(expr_list[0], expr_list[1:])])
            merged_exprs_list.append(x)

    return merged_exprs_list


def generate_recursive_expression(first_element, remaining_elements):
    op, expr = first_element
    if remaining_elements == []:
        return compile_exp_expr(expr, op)
    return PLUS, [compile_exp_expr(expr, op), generate_recursive_expression(remaining_elements[0], remaining_elements[1:])]


def compile_exp_expr(expr, op):
    ''' This function compile the #t term into delta or minus-delta '''
    operation, rvalue = expr
    t_operator, rvalue = rvalue
    assert t_operator == '#t'
    if op == INCREASE:
        delta = get_predicate_indx(DELTA)
    elif op == DECREASE:
        delta = get_predicate_indx(MINUS_DELTA)
    return (operation, [delta, rvalue])


def generate_copyall_simulate_processes_actions(g_p, g_e, g_funct, comp_scheme, plan_data, makespan):
    if comp_scheme == COPYALL:
        start_simulate_process = [generate_start_simulate_processes(g_e, g_p, g_funct)]
        
    elif comp_scheme == COPYALLplus:  # ad-hoc optimization for validating a plan
        start_simulate_process = get_set_of_start_simulate(g_e, g_p, g_funct, plan_data, makespan)

    A_P_actions            = generate_simulate_action_processes(g_p)
    end_simulate_process   = [generate_end_simulate_processes(g_e, g_funct)]

    return start_simulate_process + A_P_actions + end_simulate_process


def generate_start_simulate_processes(g_e, g_p, g_funct):
    name = START_SIMULATE
    indx_start_simulate = update_grounded_action(name, [])

    pre_list = [(NOT, get_predicate_indx(PAUSE))]
    if check_exists_event(g_e):
        pre_list.append((NOT, get_predicate_indx(FORCE_EVENTS)))

    # indx_pf = get_predicate_indx(PROP_FALSE)
    # if indx_pf != NOT_FOUND:
    #     pre_list.append((NOT, indx_pf))

    pre = AND, pre_list

    relevant_g_funct = compute_relevant_vars(g_p, g_funct)  # disabilitato

    assignment_list = [generate_assign_list(f_indx) for f_indx in relevant_g_funct]
    assignment_list.append(get_predicate_indx(PAUSE))
    assignment_list.append(get_first_done_pred_indx())
    assignment_list.append((INCREASE, [TOTAL_TIME, get_predicate_indx(DELTA)]))
    eff = AND, assignment_list
    return indx_start_simulate, pre, eff


def compute_relevant_vars(g_p, g_funct):
    '''
    Function that returns the set of relevant variables in 
    the planning task. It is necessary to avoid the UNDEFINED
    assignement
    '''

    # REFACTOR : make a dedicated structure for numeric postconditions
    relevant_vars = set()
    for p_indx, pre_p, eff_p in g_p:
        nps = eff_p[RVALUE]
        for np in nps:
            np_leaves = set()
            extract_leaves_full(np, np_leaves)
            relevant_vars = relevant_vars | np_leaves

        pre_p_leaves = set()
        extract_leaves_full(pre_p, pre_p_leaves)
        relevant_vars = relevant_vars | pre_p_leaves

    relevant_vars = relevant_vars & g_funct  # rimuovo eventuali variabili proposizionali (refactor)
    return relevant_vars


def generate_simulate_action_processes(g_p):
    action_list = []
    for p_indx, pre_p, eff_p in g_p:
        
        sub_pre_p = recursive_substitute_variables(pre_p)
        
        p_name = get_something_name(p_indx, None)
        
        assert eff_p[LVALUE] == AND
        nps = eff_p[RVALUE]  # list of numeric postconditions

        for indx_np, eff_np in enumerate(nps, start=0):
            sub_eff_np = substitute_variables(eff_np)

            if USE_CONDEFF_PROC:
                action_list.append(generate_single_simulate_process(
                    p_indx, indx_np, sub_pre_p, sub_eff_np, p_name))
            else:
                action_list += generate_couple_simulate_process(
                    p_indx, indx_np, sub_pre_p, sub_eff_np, p_name)

    action_list = add_fixed_order(action_list)

    return action_list


def add_fixed_order(action_list):
    if USE_CONDEFF_PROC:
        for indx, (_, pre_a, eff_a) in enumerate(action_list, start=0):
            pre_rvalue = pre_a[RVALUE]
            pre_rvalue.append(get_done_pred_indx(indx))
            
            eff_rvalue = eff_a[RVALUE]
            eff_rvalue.append((NOT, get_done_pred_indx(indx)))
            eff_rvalue.append(get_done_pred_indx(indx + 1))
    else:
        new_action_list = []
        indx = 0
        for couple_of_action in action_list:

            for a in couple_of_action:
                _, pre_a, eff_a = a
                pre_rvalue = pre_a[RVALUE]
                pre_rvalue.append(get_done_pred_indx(indx))
                
                eff_rvalue = eff_a[RVALUE]
                eff_rvalue.append((NOT, get_done_pred_indx(indx)))
                eff_rvalue.append(get_done_pred_indx(indx + 1))

                new_action_list.append(a)

            indx += 1
        action_list = new_action_list
    return action_list



def generate_single_simulate_process(p_indx, indx_np, sub_pre_p, sub_eff_np, p_name):           
    name_action     = SIMULATE.format(p_name, str(indx_np))
    indx_new_action = update_grounded_action(name_action, []) 
    when_single_eff = WHEN, [sub_pre_p, sub_eff_np]
    pre_preds = [get_predicate_indx(PAUSE)]
    eff_preds = [when_single_eff]
    return (indx_new_action, (AND, pre_preds), (AND, eff_preds))


def generate_couple_simulate_process(p_indx, indx_np, sub_pre_p, sub_eff_np, p_name):
    '''
    Function that generates a couple of actions for 
    each numeric postconditions of a process. One apply
    when Pre(p) holds the other otherwise.
    '''

    name_action_pos = SIMULATE_POSITIVE.format(p_name, str(indx_np))
    name_action_neg = SIMULATE_NEGATIVE.format(p_name, str(indx_np))
    indx_sim_pos    = update_grounded_action(name_action_pos, [])
    indx_sim_neg    = update_grounded_action(name_action_neg, [])

    pre_list_pos = [get_predicate_indx(PAUSE)]

    # add the process precondition just if it is not empty
    if sub_pre_p != PARSED_EMPTY_PRECONDITION:
        pre_list_pos.append(sub_pre_p)

    pre_list_neg = [get_predicate_indx(PAUSE)]
    pre_list_neg.append((NOT, sub_pre_p))

    pre_pos = AND, pre_list_pos
    pre_neg = AND, pre_list_neg

    eff_list_pos = [sub_eff_np]

    eff_list_neg = []

    eff_pos = AND, eff_list_pos
    eff_neg = AND, eff_list_neg

    active_process     = indx_sim_pos, pre_pos, eff_pos
    not_active_process = indx_sim_neg, pre_neg, eff_neg

    # if the process precondition is empty then return just the positive action
    if sub_pre_p != PARSED_EMPTY_PRECONDITION:
        return [[active_process, not_active_process]]
    else:
        return [[active_process]]


def substitute_variables(single_eff):
    '''
    IMPORTANTE
    '''
    lvalue = single_eff[LVALUE]
    rvalue = single_eff[RVALUE]
    affected_variable = rvalue[LVALUE]
    expr = rvalue[RVALUE:]
    sub_expr  = recursive_substitute_variables(expr)
    full_expr = [affected_variable] + sub_expr
    return lvalue, full_expr 


def recursive_substitute_variables(expr):
    if type(expr) == int:    # variable indx
        if is_function(expr):
            return get_copy_variable_from_indx(expr)
        else:
            return expr  # if it is a propositional variable
    elif type(expr) == str:  # #t or costant
        if expr == CONTINOUS_TIME:
            return get_predicate_indx(DELTA)
        else:
            return expr
    elif type(expr) == list:
        return [recursive_substitute_variables(el) for el in expr]

    elif type(expr) == tuple and len(expr) == 2:
        lvalue = expr[LVALUE]
        rvalue = expr[RVALUE]
        if type(rvalue) == int:
            assert lvalue == NOT
            return NOT, recursive_substitute_variables(rvalue)
        else:
            return lvalue, [recursive_substitute_variables(el) for el in rvalue]
    else:
        assert False


def generate_end_simulate_processes(g_e, g_p):
    name = END_SIMULATE
    indx_end_simulate = update_grounded_action(name, [])

    pause_indx = get_predicate_indx(PAUSE)

    pre_preds = [pause_indx, get_last_done_pred_indx()]
    pre = AND, pre_preds

    eff_preds = [(NOT, pause_indx)] + [(NOT, get_last_done_pred_indx())]
    if check_exists_event(g_e):
        eff_preds += [get_predicate_indx(FORCE_EVENTS)]

    eff = AND, eff_preds

    return indx_end_simulate, pre, eff    



def generate_assign_list(f_indx):
    return ASSIGN, [get_copy_indx_of_indx(f_indx), f_indx]


def get_copy_indx_of_indx(f_indx):
    name = get_predicate_name(f_indx)
    return get_predicate_indx(COPY.format(name))


def generate_simulate_processes(g_p, g_e, invalid_block_same_lvalues):
    '''
    Function which generates a monolithic action to compile away the 
    processes. 
    Used by compilation scheme: INVALID
    '''
    exists_e = check_exists_event(g_e)

    if exists_e:
        # force_events_indx = get_action_indx(FORCE_EVENTS)
        force_events_indx = get_predicate_indx(FORCE_EVENTS)


    indx_action = update_grounded_action(SIMULATE_PROCESSES, [])

    if exists_e:
        pre  = AND, (NOT, force_events_indx)
    else:
        pre  = AND, PARSED_EMPTY_PRECONDITION

    when_list = generate_simulate_processes_conditional_effects(g_p, invalid_block_same_lvalues)

    eff_list = when_list
    if exists_e: 
        eff_list = eff_list + [force_events_indx]
    eff = AND, eff_list
    return (indx_action, pre, eff)


def generate_simulate_processes_conditional_effects(g_p, invalid_block_same_lvalues):
    '''
    Functions which generates a list of conditional effect
    for each numeric effect of each process
    '''
    when_list = []
    delta_indx = get_predicate_indx(DELTA)

    for (indx_p, pre_p, post_p) in g_p:
        lvalue, rvalue = post_p[LVALUE], post_p[RVALUE]
        assert lvalue == AND
        when_p_eff_list = []
        for process_effect in rvalue:
            op, affected_variable, expr = unpack_process_effect(process_effect)
            when_p_eff = op, [affected_variable, (TIMES, [expr, delta_indx])]
            when_p_eff_list.append(when_p_eff)

        if pre_p == PARSED_EMPTY_PRECONDITION:
            when_p = when_p_eff_list
        else:
            when_p = WHEN, [pre_p, (AND, when_p_eff_list)]

        when_list.append(when_p)

    ''' add conditional effects to make the compilation scheme sound '''
    when_list = when_list + generate_block_same_lvalues_condeff(invalid_block_same_lvalues, g_p)

    when_list.append((INCREASE, [TOTAL_TIME, delta_indx]))

    return when_list



def generate_block_same_lvalues_condeff(invalid_block_same_lvalues, g_p):
    if invalid_block_same_lvalues is False:
        return []

    indx_undefined = get_predicate_indx(UNDEFINED)

    ''' REFACTOR this part - create list of  numeric preconditions '''
    nps = []
    for (indx_p, pre_p, post_p) in g_p:
        _, rvalue = post_p[LVALUE], post_p[RVALUE]
        when_p_eff_list = []
        for process_effect in rvalue:
            op, affected_variable, expr = unpack_process_effect(process_effect)

            nps.append((affected_variable, pre_p))

    block_when_list = []
    for indx_np1, np1 in enumerate(nps):
        for indx_np2, np2 in enumerate(nps):
            if indx_np2 < indx_np1:
                continue
            x1, pre_1 = np1
            x2, pre_2 = np2
            if x1 == x2 and np1 != np2:
                cond = AND, [pre_1, pre_2]
                eff  = indx_undefined
                block_when_list.append((WHEN, [cond, eff]))

    return block_when_list


def unpack_process_effect(process_effect):
    '''
    Function with extract some structure from
    a numeric process effect.
    Process effect has the following form:
        [NUM_OP, [AFFECTED_VARIABLE, [TIMES, [#t, EXPR]]]]
    where:
        - NUM_OP in {increase, decrease}
    '''
    num_op = process_effect[LVALUE]
    assert num_op in [INCREASE, DECREASE]
    affected_variable = process_effect[RVALUE][LVALUE]
    times = process_effect[RVALUE][RVALUE][LVALUE]
    assert times == TIMES
    expr = process_effect[RVALUE][RVALUE][RVALUE][RVALUE]
    return num_op, affected_variable, expr


def compile_problem(g_problem_dict, g_funct, g_e, comp_scheme, val_schema, delta):
    goal_state = g_problem_dict[GOAL_STATE]
    

    if comp_scheme != 'plain':
        lvalue = goal_state[LVALUE]
        rvalue = goal_state[RVALUE]

        rvalue.append((NOT, get_predicate_indx(PAUSE)))

        if check_exists_event(g_e):
            rvalue.append((NOT, get_predicate_indx(FORCE_EVENTS)))

            if comp_scheme == INVALID:
                rvalue.append((NOT, get_predicate_indx(UNDEFINED)))

        g_problem_dict[GOAL_STATE] = lvalue, rvalue

        g_problem_dict[INIT_STATE] = compile_initial_state(g_problem_dict, g_funct, g_e, comp_scheme, delta)
        g_problem_dict[INIT_STATE].append((EQUAL, [TOTAL_TIME, '0.0']))


    return g_problem_dict


def compile_initial_state(g_problem_dict, g_funct, g_e, comp_scheme, delta):
    ''' compiling the initial state '''

    init_state = g_problem_dict[INIT_STATE]  # DELTA

    init_state.append((EQUAL, [get_predicate_indx(DELTA), str(delta)]))

    if comp_scheme in [EXP, INVALIDplus]:
        init_state.append((EQUAL, [get_predicate_indx(MINUS_DELTA), str(0 - float(delta))]))


    if comp_scheme in [COPYALL, COPYALLplus]:
        # SISTEMARE
        init_state = add_copy_variables_initial_state(init_state, g_funct)
        init_state = fix_undefined_vars(init_state, g_funct)
    if check_exists_event(g_e):
        init_state.append(get_predicate_indx(FORCE_EVENTS))

    return init_state


def add_copy_variables_initial_state(init_state, g_funct):
    new_assignements = []
    for x in init_state:
        if type(x) == tuple: # it is an assignment
            lvalue = x[LVALUE]
            if lvalue == NOT:   # ignore if there is an assignment with NOT (closed world assumption)
                continue
            assert lvalue == EQUAL
            rvalue = x[RVALUE]
            assert len(rvalue) == 2
            variable = rvalue[LVALUE]
            value    = rvalue[RVALUE]
            copy_var = get_copy_variable_from_indx(variable)
            copy_assignment = (EQUAL, [copy_var, value])
            new_assignements.append(copy_assignment)
    init_state = init_state + new_assignements
    return init_state


def fix_undefined_vars(init_state, g_funct):
    '''
    Function that assigns to each undefined numeric variable in the
    initial state a standard value of  0
    '''

    numeric_init_state = [i for i in init_state if type(i) != int]  # consider only numeric variables
    # print (i) - > (=, [lvalue, rvalue])
    assigned_vars = [i[RVALUE][LVALUE] for i in numeric_init_state]
    new_assignements = []

    for indx_var in g_funct:
        name = get_predicate_name(indx_var)
        # Bad solution. If indx_var is a copy variable, I 
        if indx_var not in assigned_vars and 'copy--' not in get_predicate_name(indx_var):
            new_assignements.append((EQUAL, [indx_var, str(0)]))
            new_assignements.append((EQUAL, [get_predicate_indx(get_copy_function(indx_var)), str(0)]))
    init_state = init_state + new_assignements
    return init_state


def get_set_of_start_simulate(g_e, g_p, g_funct, plan_data, makespan):

    grouped_action, seen_timestamp = get_timed_actions(plan_data, makespan)

    ''' refactor this part, merge with another function for generate START-SIMULATE in compiler.py '''

    new_actions = []


    for indx_simulate, timed_actions in enumerate(grouped_action):
        pre_list = []

        name = START_SIMULATE_t.format(indx_simulate)
        indx_start_simulate = update_grounded_action(name, [])
        pre_list.append((NOT, get_predicate_indx(PAUSE)))
        if check_exists_event(g_e):
            pre_list.append((NOT, get_predicate_indx(FORCE_EVENTS)))

        # PRECONDIZIONI ESTESE PER LA i-esima SIMULATE-time
        for i_ta in timed_actions:
            pre_list.append(get_predicate_indx(MONO_EVENT_DONE.format(i_ta)))

        if indx_simulate < len(seen_timestamp) - 1:
            pre_list.append(
                    (GREATEREQ, [TOTAL_TIME, str(seen_timestamp[indx_simulate])]))
            pre_list.append(
                    (LESS, [TOTAL_TIME, str(seen_timestamp[indx_simulate + 1])]))                
            pre = AND, pre_list


            relevant_g_funct = compute_relevant_vars(g_p, g_funct)  # disabilitato
            # relevant_g_funct = g_funct

            assignment_list = [generate_assign_list(f_indx) for f_indx in relevant_g_funct]
            assignment_list.append(get_predicate_indx(PAUSE))
            assignment_list.append(get_first_done_pred_indx())
            assignment_list.append((INCREASE, [TOTAL_TIME, get_predicate_indx(DELTA)]))
            eff = AND, assignment_list
            if seen_timestamp[indx_simulate] < float(makespan):
                new_actions.append((indx_start_simulate, pre, eff))

    return new_actions



# STRING FUNCTIONS #

def get_fired_pred(e_indx):
    return FIRED.format(get_something_name(e_indx, None))


def get_done_pred(indx):
    return DONE.format(str(indx))
