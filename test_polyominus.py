import re
import subprocess

from os      import listdir, system
from os.path import join, basename, dirname

MAX_SOLVED = {
    'Solar-Rover'    : ['prob01.pddl'],
    'Linear-Car'     : ['instance_1_30.0_0.1_10.0.pddl', 
                        'instance_2_30.0_0.1_10.0.pddl', 
                        'instance_3_30.0_0.1_10.0.pddl', 
                        'instance_4_30.0_0.1_10.0.pddl', 
                        'instance_5_30.0_0.1_10.0.pddl', 
                        'instance_6_30.0_0.1_10.0.pddl', 
                        'instance_7_30.0_0.1_10.0.pddl', 
                        'instance_8_30.0_0.1_10.0.pddl', 
                        'instance_9_30.0_0.1_10.0.pddl', 
                        'instance_10_30.0_0.1_10.0.pddl'],
    'Linear-Generator': ['instance_1.pddl', 
                         'instance_2.pddl'
                         'instance_3.pddl'
                         'instance_4.pddl'
                         'instance_5.pddl'
                         'instance_6.pddl'
                         'instance_7.pddl'
                         'instance_8.pddl'],
    'Baxter'          : ['P4_i1', 'P4_i2' , 'P4_i3', 'P4_i4', 'P4_i5', 'P5_i3', 'P6_i1', 'P6_i2'],
    'OT-Car'          : ['instance_01.pddl', 
                         'instance_02.pddl', 
                         'instance_03.pddl',
                         'instance_04.pddl',
                         'instance_05.pddl'],
    'Descent'         : ['prob_earth01.pddl',
                         'prob_earth02.pddl',
                         'prob_earth03.pddl',
                         'prob_earth04.pddl']
}

PLANNERS = ['enhsp20']

''' PARAMETERS '''

DOMAINS = [

    'Solar-Rover',      # DID FULL
    'Linear-Car',       # DID FULL
    'Linear-Generator', # DID FULL
    'Baxter', 
    'OT-Car',           # DID FULL
    # 'UTC', 
    'HVAC', 
    'Descent', 
]

DOMAINS = ['Solar-Rover']

Hs = ['opt-blind', 'opt-hmax']
Hs = ['opt-hmax']

SCHEMA   = ['copyall', 'invalidplus', 'exp', 'invalid']
SCHEMA   = ['invalid']

''' END PARAMETERS '''

TIME_COMMAND = '/usr/bin/time -f "Total-Time: %e" '

ENHSP20_template  = 'timeout {} enhsp20/enhsp_LAST -o {} -f {} -d 1 -planner {} > enhsp20_temp_log 2>&1'
PDDLPLUS_COMPILER = 'python3 pddlplus-translator/pddlplus_compiler.py --domain_path {} --problem_path {} --output_path {} --schema {} --delta \'{}\' --no_event_opt_cascade False --no_event_opt_action False  > translator_log 2>&1'

DOMAINS_PATH = 'pddlplus-translator/ICAPS2021_benchmarks/{}'
PLAN_PATH    = 'enhsp20-plans[delta=0.1]/{}/{}'

UNSOLVED = None

def main():
    INSTANCES = generate_instances_dict()

    for h in Hs:
        print (h)
        for d in DOMAINS:
            print ('\t' + d)
            d_h_log = ''
            for s in SCHEMA:
                print ('\t\t'+ s)
                for dname, iname in INSTANCES[d]:
                    print ('\t\t\t' + iname.split('/')[-1])
                    c_dname, c_iname, translator_log = compile(dname, iname, s)
                    lazy_walltime    = get_lazy_walltime(d, iname, h)
                    enhsp_command    = ENHSP20_template.format(lazy_walltime, c_dname, c_iname, h)

                    system(TIME_COMMAND + enhsp_command)
                    log              = readfile('enhsp20_temp_log')
                    comp_time        = extract_total_time(translator_log)
                    system('rm enhsp20_temp_log')
                    system('rm {}'.format(c_dname))
                    system('rm {}'.format(c_iname))
                    if 'Problem unsolvable' not in log:
                        e_nodes          = extract_nodes(log)
                        Ntot             = extract_ntot(translator_log)
                        Nmax             = extract_nmax(translator_log)
                        makespan         = extract_makespan(log, s)
                        planning_time    = extract_total_time(log)
                        total_time       = comp_time + planning_time
                    else:
                        e_nodes, Ntot, Nmax, makespan, planning_time, total_time = '---', '---', '---', '---', '---', '---'

                    # print ('enode: {}'.format(e_nodes))
                    row              = 'domain={}'.format(d) + ' dname={}'.format(get_last_name(dname)) + ' iname={}'.format(get_last_name(iname)) + ' schema={}'.format(s) + ' exp_nodes={}'.format(e_nodes) + ' Ntot={}'.format(str(Ntot)) + ' Nmax={}'.format(str(Nmax)) + ' makespan={}'.format(str(makespan)) + ' comp_time={}'.format(str(comp_time)) + ' planning_time={}'.format(planning_time) + ' total_time={}'.format(total_time)
                    d_h_log += row + '\n'
                    # print (row + '\n')
            ''' SAVE '''
            with open(join('poly_vs_explocal/{}/{}'.format(h, d)), 'w') as fout:
                fout.write(d_h_log)
            #print ('LOG2SAVE')
            #print (d_h_log)


def get_lazy_walltime(d, iname, h):
    last_iname = get_last_name(iname)
    if d != 'HVAC':
        if last_iname in MAX_SOLVED[d]:
            return str(180)
        else:
            return str(1)
    elif d == 'HVAC':
        if h == 'opt-blind':
            return str(1)
        elif h == 'opt-hmax':
            return str(180)


def extract_total_time(log):
    total_time = (re.findall('Total-Time:[^\n]+', log))
    assert len(total_time) == 1
    return float(total_time[0].split('Total-Time: ')[1])


def extract_makespan(log, s):
    if s in ['invalid', 'invalidplus']:
        return len(re.findall('simulate-process', log))
    elif s in ['copyall']:
        return len(re.findall('start-simulate-processes', log))
    elif s in ['exp']:
        return len(re.findall('simulate_processes_', log))


def get_last_name(path):
    return (path.split('/')[-1])


def compile(dname, iname, s):

    domain_path  = dname
    problem_path = iname
    output_path  = 'pddlplus-translator/OUTPUT'
    schema       = s
    delta        = '1'

    compiler_command = PDDLPLUS_COMPILER.format(domain_path, problem_path, output_path, schema, delta)
    system(TIME_COMMAND + compiler_command)
    print (TIME_COMMAND + compiler_command)
    exit(1)
    translator_log   = readfile('translator_log')
    system('rm translator_log')
    last_dname = dname.split('/')[-1]
    last_iname = iname.split('/')[-1]
    return join(output_path, 'compiled_{}'.format(last_dname)), join(output_path, 'compiled_{}'.format(last_iname)), translator_log


def extract_ntot(log):
    exp_string = (re.findall(r'Ntot:[^\n]+\n', log))
    if len(exp_string) == 0:
        return '---'
    else:
        assert len(exp_string) == 1
        return (exp_string[0].split(':')[1]).replace('\n', '')


def extract_nmax(log):
    exp_string = (re.findall(r'Nmax:[^\n]+\n', log))
    if len(exp_string) == 0:
        return '---'
    else:
        assert len(exp_string) == 1
        return (exp_string[0].split(':')[1]).replace('\n', '')


def extract_nodes(log):
    exp_string = (re.findall(r'Expanded Nodes:\d+[^\n]', log))
    if len(exp_string) == 0:
        return '---'
    else:
        assert len(exp_string) == 1
        return (exp_string[0].split(':')[1])


def generate_instances_dict():
    instances_dict = {}
    for d in DOMAINS:
        dpath = DOMAINS_PATH.format(d)
        files = listdir(dpath)
        instances_dict[d] = separate_files(dpath, files)
    return instances_dict


def separate_files(fulldpath, files):
    if 'domain_UTC.pddl' in files:
        files.remove('domain_UTC.pddl')

    problems = [x for x in files if x != 'domain.pddl']
    domains  = [x for x in files if x not in problems]

    assert len(domains) == 1
    domains = [domains[0] for x in range(0, len(problems))]
    problems = natural_sort(problems)
    tups = [(join(fulldpath, domains[indx]), join(fulldpath, problems[indx])) for indx in range(0, len(problems))]
    return tups


def extract_enhsp_solution_from(log):

    if 'Problem Solved' not in log:
        return UNSOLVED

    log = log.split('Problem Solved')[1]

    log = log.split('Plan-Length:')[0]

    plan = '\n'.join([r for r in log.split('\n') if r != ''])

    return plan 


def save_plan(plan, d, ppath):
    pname = basename(ppath)
    plan_path = PLAN_PATH.format(d, pname)
    savefile(plan_path.replace('.pddl', '.plan'), plan)



def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def readfile(path):
    with open(path, 'r') as fout:
        return fout.read()


def savefile(path, plan):
    with open(path, 'w') as fout:
        fout.write(plan)



if __name__ == "__main__":
    main()