from utils           import *
from global_map      import *
from nltk_preprocess import *


def parse_domain(domain_input):
    global baxter_cascading

    print ('Parsing domain')

    domain_str = readfile(domain_input)

    # TEMPORARY [TROVARE DEFINIZIONE DI CASCADING EVENTS]
    set_baxter_cascading(domain_str)

    domain_str = nltk_domain_preprepocess(domain_str)
    
    t = nltk.tree.Tree.fromstring(domain_str)

    types       = []
    processes   = []
    events      = []
    actions     = []

    for a in t:
        if is_tree(a):

            a_label = a.label()

            if a_label == DOMAIN_KW:
                domain_name = parse_domain_name(a)

            elif a_label == TYPES_KW:
                types = parse_domain_types(a)

            elif a_label == PREDICATES_KW:
                predicates = parse_domain_predicates(a)

            elif a_label == FUNCTIONS_KW:
                functions = parse_domain_functions(a)

            elif a_label == PROCESS_KW:
                processes.append(parse_domain_process(a))

            elif a_label == EVENTS_KW:
                events.append(parse_domain_process(a))

            elif a_label == ACTION_KW:
                actions.append(parse_domain_process(a))

    # int_types = update_types(string_types)  # useless
    invariants   = get_lifted_prop_invariants(predicates + functions, events, actions, processes)

    domain_dict = {
        DOMAIN_NAME : domain_name,
        TYPES       : types,
        PREDICATES  : predicates,
        FUNCTIONS   : functions,
        PROCESSES   : processes,
        EVENTS      : events,
        ACTIONS     : actions,
        INVARIANTS  : invariants
    }

    return domain_dict


def parse_plan(plan_path):
    '''
    Example of action:
    9.0: (stop_car car1)
    '''
    SEP = ':'

    plan      = readfile(plan_path)
    plan_data = []
    makespan = 0.0
    for ts_action in (plan.split('\n')):
        if ts_action == '':
            continue
        s_ts_action = (ts_action.split(SEP))
        timestamp   = float(s_ts_action[0])
        action      = s_ts_action[1]
        action_regex = re.findall(r'\([^\(]+\)', action)
        if len(action_regex) == 1:
            action = action_regex[0].replace('(', '').replace(')', '')
        else:
            time = (re.findall(r'\[[^\[]+\]', action))
            assert len(time) == 1
            makespan = time[0].replace('[', '').replace(']', '')
            continue

        plan_data.append((timestamp, action))

    return plan_data, makespan


def get_lifted_prop_invariants(predicates, events, actions, processes):
    invariants = set()
    for pred_name, _ in predicates:
        is_invariant = INVARIANT
        for _, _, _, a_eff in actions + events + processes:
            if pred_name in get_lifted_predicates(a_eff):
                is_invariant = not INVARIANT
        if is_invariant is INVARIANT:
            invariants.add(pred_name)
    return invariants


def get_lifted_predicates(formula):
    leaves_formula = set()
    extract_lifted_leaves(formula, leaves_formula)
    return set([e for e in leaves_formula if not not_variable(e) and '?' not in e])


def parse_domain_name(t):
    domain_name = [a for a in t if not is_tree(a)]
    assert len(domain_name) == 1
    return domain_name[0]


def parse_domain_types(t):
    return [a for a in t if not is_tree(a)]



def parse_domain_predicates(t):
    return [parse_domain_predicate(a) for a in t if is_tree(a)]


def parse_domain_predicate(t):
    name_predicate = t.label()
    pars = []
    count_obj = 0
    for a in t:
        if '?' in a:
            count_obj += 1
        elif '-' in a:
            a = a.replace('-', '')
            if a == '':
                pass
            else:
                pars = pars + [a for i in range(0, count_obj)]
                count_obj = 0
        else:
            pars = pars + [a for i in range(0, count_obj)]
            count_obj = 0

    return (name_predicate, pars)


def parse_domain_functions(t):
    return [parse_domain_predicate(a) for a in t if is_tree(a)]


def parse_domain_process(t):

    parameters   = []
    # precondition = []
    # effect       = []
    name         = None
    for a in t:
        if is_tree(a):
            

            if a.label() == PARAMETERS_KW:
                parameters = parse_domain_parameters(a)

            elif a.label() == PRECONDITION_KW:
                precondition = parse_domain_precondition(a)

            elif a.label() == EFFECT_KW:
                effect = parse_domain_effect(a)

        else:
            name = a

    return (name, parameters, precondition, effect)


def parse_domain_parameters(t):
    pars      = []
    variables = []
    for a in t:
        if '?' in a:
            variables.append(a)
        elif '-' in a:
            a = a.replace('-', '')
            if a != '':
                pars.append((variables, a))
                variables = []
            pass
        else:
            pars.append((variables, a))
            variables = []

    return pars


def parse_domain_precondition(a):
    pre = parse_domain_formula(a)[0]
    
    lvalue = pre[0]

    # fix if there is just one effect without AND
    if pre == PARSED_EMPTY_PRECONDITION:
        return PARSED_EMPTY_PRECONDITION

    elif lvalue != AND:
        return (AND, [pre])
    else:
        return pre


def parse_domain_effect(a):
    eff = parse_domain_formula(a)[0]
    lvalue = eff[0]
    # fix if there is just one effect without AND
    if lvalue != AND:
        eff = AND, [eff]
    return eff



def is_numeric_condition(pre):
    first_element = pre[0]
    return first_element in NUMERIC_OPERATORS


def parse_domain_formula(t):
    to_add = []
    for a in t:
        if is_tree(a):
            to_add.append((a.label(), parse_domain_formula(a)))
        else:
            to_add.append(a)

    return to_add


# FUNZIONI GESTIONE PROBLEMA #
def parse_problem(problem_input, types):
    '''
    Function which parses the problem definition
    '''
    print ('Parsing problem\n')

    problem_str = readfile(problem_input)

    problem_str = nltk_problem_preprepocess(problem_str)

    problem_name = parse_problem_name(problem_str)

    objects = parse_objects(problem_str, types)

    init    = parse_initial_state(problem_str)

    goal    = parse_goal_state(problem_str)

    problem_dict = {
        PROBLEM_NAME: problem_name,
        OBJECTS     : objects,
        INIT_STATE  : init,
        GOAL_STATE  : goal,
    }

    return problem_dict


def parse_objects(problem_str, types):
    objects_str = get_sub_problem('(:objects', problem_str)
    
    # se il problema non ha oggetti:
    if objects_str == NOT_FOUND:
        return ''

    '''
        INPUT : (:objects r1 - room k1  -request)
        OUTPUT: (:objects r1 - room k1 - request)
    '''
    no_spaces = (re.findall(' +-[^ ]', objects_str))
    for x in no_spaces:
        objects_str = objects_str.replace(x, x[0:-1] + ' ' + x[-1])

    t = nltk.tree.Tree.fromstring(objects_str)

    objects = {}
    o2add = []
    for a in t:
        if a != '-':
            if a in types:
                objects[a] = o2add
                o2add = []
            else:
                o2add.append(a)
    return objects


def parse_initial_state(problem_str):
    '''
    TO REFACTOR
    '''
    init_str = get_sub_problem('(:init', problem_str)
    t = nltk.tree.Tree.fromstring(init_str)
    return parse_domain_formula(t)
    # init_state = initialize_init_state()
    # indx = 0
    # # newline nella dichiarazione init, irrobustici!
    # for p in init_str.split('\n'):
    #     if ':init' in p or p == '':
    #         continue
    #     try:
    #         t = nltk.tree.Tree.fromstring(p)
    #         init_state = parse_initial_state_pred(t, init_state)
    #     except:
    #         pass
    # return init_state


# def parse_initial_state_pred(t, init_state):
#     if t.label() == EQUAL:
#         assert len(t) == 2
#         var   = t[0]
#         value = t[1]
#         init_state[NUMERICAL].append((var.label(), parse_grounded_predicate(var), float(value)))

#     else:

#         init_state[PROPOSITIONAL].append((t.label(), parse_grounded_predicate(t)))
    
#     return init_state


def parse_goal_state(problem_str):
    goal_str = get_sub_problem('(:goal', problem_str)
    t = nltk.tree.Tree.fromstring(goal_str)
    return parse_domain_formula(t)[0]


def initialize_init_state():
    return {
        PROPOSITIONAL: [],
        NUMERICAL    : []
    }


def parse_grounded_predicate(t):
    objs = []
    for a in t:
        if not is_tree(a):
            objs.append(a)
    return objs


def parse_problem_name(problem_str):
    occ = re.findall(r'\(problem[^\)]+\)', problem_str)
    assert len(occ) == 1
    occ = occ[0]
    t = nltk.tree.Tree.fromstring(occ)

    for a in t:
        if not is_tree(a):
            pass
        return a


def get_sub_problem(token, problem_str):
    start = problem_str.find(token)
    if start == NOT_FOUND:
        return NOT_FOUND
    return close_brackets(start, problem_str)


def close_brackets(start, problem_str):
    indx = start + 1
    b = 1
    while True:
        if problem_str[indx] == '(':
            b += 1
        elif problem_str[indx] == ')':
            b += -1
        indx += 1

        if b == 0:
            break
    return problem_str[start:indx]


