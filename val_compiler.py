from utils                  import *
from global_map             import *
from numeric_postconditions import *


def compile_plan_into_domain(g_domain_dict, plan_data, makespan, mode, delta, schema, val_schema):
    global GLOBAL_MAP
    if mode == TRANSLATION:
        return g_domain_dict

    add_novel_predicates_val(g_domain_dict, plan_data, val_schema, schema)

    indx_false = generate_new_predicate(PROP_FALSE)
    g_domain_dict[G_PRED].add(indx_false)
    g_domain_dict = add_bot_to_everything(g_domain_dict)

    if schema == 'plain':
        '''
        ENHSP does not accept total-time in its precondition since
        total-time is a keyword. So we use novel-time.
        '''
        indx_novel_time = generate_new_function(NOVEL_TIME)
        g_domain_dict[G_FUNCT].add(indx_novel_time)
        g_domain_dict[G_P].append(generate_time_process(delta, makespan))

    if val_schema in [Z0, Z1, Z2, Z3]:
        g_domain_dict[G_A] = compile_action_into_event(g_domain_dict, plan_data, schema, val_schema)

    if val_schema in [Z1, Z2, Z3]:
        g_domain_dict[G_P] = compile_process_bound(g_domain_dict, schema, makespan)

    if val_schema == Z2: 
        if schema != COPYALLplus:
            g_domain_dict[G_E] += add_bot_events(g_domain_dict, plan_data, makespan, delta, schema)

    elif val_schema == Z3:
        g_domain_dict[G_P] = split_processes(g_domain_dict[G_P], plan_data, makespan, schema)

    ''' Removed, it is not necessary '''
    # g_domain_dict[G_E] = add_time_bound_event(g_domain_dict, makespan, schema)

    return g_domain_dict


def add_time_bound_event(g_domain_dict, makespan, schema):
    ''' TEMPORARY FUNCTION '''
    if schema == 'plain':
        time2add = get_predicate_indx(NOVEL_TIME)
    else:
        time2add = TOTAL_TIME

    indx_bound = update_grounded_action(BOUND_EVENT, [])
    pre = AND, [(NOT, get_predicate_indx(PROP_FALSE)), (GREATER, [time2add, str(float(makespan))])]
    eff = AND, [get_predicate_indx(PROP_FALSE)]
    return [(indx_bound, pre, eff)]


def compile_process_bound(g_domain_dict, schema, makespan):
    if schema == 'plain':
        time2add = get_predicate_indx(NOVEL_TIME)
    else:
        time2add = TOTAL_TIME

    for indx_list, p in enumerate(g_domain_dict[G_P]):
        indx, pre, eff = p
        op, pre_list = pre
        if op == AND:
            if schema == 'plain':
                pre_list.append((LESS, [time2add, makespan]))
            else:
                assert schema in [COPYALL, COPYALLplus]
                pre_list.append((LESSEQ, [time2add, makespan]))
        elif pre == PARSED_EMPTY_PRECONDITION:
            if schema == 'plain':
                pre = AND, [(LESS, [time2add, makespan])]
            else:
                assert schema in [COPYALL, COPYALLplus]
                pre = AND, [(LESSEQ, [time2add, makespan])]

            g_domain_dict[G_P][indx_list] = indx, pre, eff
        else:
            print ('Extend Code. Function: compile_process_bound')
            exit(1)
    return g_domain_dict[G_P]


def split_processes(g_p, plan_data, makespan, val_schema):
    new_gp = []
    timed_actions, seen_timestamp = get_timed_actions(plan_data, makespan)

    if val_schema == 'plain':
        indx_time = get_predicate_indx(NOVEL_TIME)
    else:
        indx_time = TOTAL_TIME

    for indx_ts, ts in enumerate(seen_timestamp):
        if indx_ts == len(seen_timestamp) - 1:
            break

        ts_action = timed_actions[indx_ts]
        pre_list  = [get_predicate_indx(MONO_EVENT_DONE.format(i)) for i in ts_action]

        if val_schema == 'plain':
            pre_list.append((GREATEREQ, [indx_time, str(ts)]))
        else:
            pre_list.append((GREATER, [indx_time, str(ts)]))

        if indx_ts < len(seen_timestamp) - 1:
            if val_schema == 'plain':
                pre_list.append((LESS, [indx_time, str(seen_timestamp[indx_ts + 1])]))
            else:
                pre_list.append((LESSEQ, [indx_time, str(seen_timestamp[indx_ts + 1])]))

        for indx_p, pre_p, eff_p in g_p:
            new_name  = get_something_name(indx_p, None) + '--step--{}'.format(indx_ts)
            new_pre_p = AND, pre_p[RVALUE] + pre_list
            new_eff_p = eff_p
            new_indx  = update_grounded_action(new_name, [])
            new_gp.append((new_indx, new_pre_p, new_eff_p))
    
    return new_gp


def add_bot_to_everything(g_domain_dict):
    ''' actions '''
    for indx_s, a_tup in enumerate(g_domain_dict[G_A]):
        indx_a, pre_a, eff_a = a_tup
        if pre_a[LVALUE] == AND:
            pre_a[RVALUE].append((NOT, [get_predicate_indx(PROP_FALSE)]))
        elif pre_a == PARSED_EMPTY_PRECONDITION:
            g_domain_dict[G_A][indx_s] = indx_a, (AND, [(NOT, [get_predicate_indx(PROP_FALSE)])]), eff_a
        else:
            print (pre_a)
            print ('Please extend code in \'add_bot_to_everything - action\'')
            exit(1)
    ''' processes '''
    for indx_s, p_tup in enumerate(g_domain_dict[G_P]):
        indx_p, pre_p, eff_p = p_tup
        if pre_p[LVALUE] == AND:
            pre_p[RVALUE].append((NOT, get_predicate_indx(PROP_FALSE)))
        elif pre_p == PARSED_EMPTY_PRECONDITION:
            g_domain_dict[G_P][indx_s] = indx_p, (AND, [(NOT, [get_predicate_indx(PROP_FALSE)])]), eff_p
        else:
            print (pre_p)
            print ('Please extend code in \'add_bot_to_everything\' - process')
            exit(1)

    ''' events '''
    # for indx_s, e_tup in enumerate(g_domain_dict[G_E]):
    #     indx_e, pre_e, eff_e = e_tup
    #     if pre_p[LVALUE] == AND:
    #         pre_e[RVALUE].append((NOT, get_predicate_indx(PROP_FALSE)))
    #     elif pre_p == PARSED_EMPTY_PRECONDITION:
    #         g_domain_dict[G_A][indx_s] = indx_e, (AND, (NOT, get_predicate_indx(PROP_FALSE))), eff_e
    #     else:
    #         print (pre_p)
    #         print ('Please extend code in \'add_bot_to_everything\' - process')
    #         exit(1)

    return g_domain_dict


def add_bot_events(g_domain_dict, plan_data, makespan, delta, schema):

    if schema == 'plain':
        time2add = get_predicate_indx(NOVEL_TIME)
    else:
        time2add = TOTAL_TIME

    timed_actions, seen_timestamp = get_timed_actions(plan_data, makespan)
    timed_events = []
    eff_e        = AND, [get_predicate_indx(PROP_FALSE)]
    not_false    = NOT, [get_predicate_indx(PROP_FALSE)]

    for indx_ts, ts in enumerate(seen_timestamp, start=0):
        if len(timed_actions[indx_ts]) > 0:
            indx_bot     = update_grounded_action(TIMED_EVENT.format(str(len(timed_events))), [])
            greater_time = GREATER, [time2add, str(float(ts))]
            indx_a       = timed_actions[indx_ts][-1]
            not_done     = NOT, [get_predicate_indx(MONO_EVENT_DONE.format(indx_a))]
            pre_ebot     = AND, [greater_time, not_done, not_false]
            timed_events.append((indx_bot, pre_ebot, eff_e))
    return timed_events


def add_novel_predicates_val(g_domain_dict, plan_data, val_schema, schema):
    indx_prop_true  = generate_new_predicate(PROP_TRUE)
    g_domain_dict[G_PRED].add(indx_prop_true)

    for indx in range(0, len(plan_data) + 1):
        indx_test = generate_new_predicate(EVENT_DONE.format(str(indx)))
        g_domain_dict[G_PRED].add(indx_test)

        if val_schema in [Z2, Z3]:
            indx_test = generate_new_predicate(MONO_EVENT_DONE.format(str(indx)))
            g_domain_dict[G_PRED].add(indx_test)


def compile_plan_into_problem(g_problem_dict, plan_data, makespan, mode, delta, schema, val_schema):

    if schema == 'plain':
        time2add = get_predicate_indx(NOVEL_TIME)
    else:
        time2add = TOTAL_TIME

    g_problem_dict[INIT_STATE].append(get_predicate_indx(PROP_TRUE))
    g_problem_dict[INIT_STATE].append(get_predicate_indx(EVENT_DONE.format(str(0))))

    if schema == 'plain':
        g_problem_dict[INIT_STATE].append((EQUAL, [get_predicate_indx(NOVEL_TIME), '0.0']))


    goal_state = g_problem_dict[GOAL_STATE]
    lvalue = goal_state[LVALUE]
    rvalue = goal_state[RVALUE]
    rvalue.append(get_predicate_indx(EVENT_DONE.format(str(len(plan_data)))))
    rvalue.append((EQUAL, [time2add, makespan]))

    return g_problem_dict


def generate_time_process(delta, makespan):
    indx_novel_time = get_predicate_indx(NOVEL_TIME)

    indx = update_grounded_action(SIMULATE_TIME, [])
    # pre_list = [(LESS, [indx_novel_time, makespan]), (NOT, get_predicate_indx(PROP_FALSE))]
    pre_list = [(NOT, get_predicate_indx(PROP_FALSE))]


    pre  = AND, pre_list
    eff_list = []
    eff_list.append((INCREASE, [indx_novel_time, (TIMES, [CONTINOUS_TIME, str(1)])]))
    eff  = AND, eff_list

    return (indx, pre, eff)


def compile_action_into_event(g_domain_dict, plan_data, schema, val_schema):
    global GLOBAL_MAP
    actions = g_domain_dict[G_A]
    
    if schema == 'plain':
        indx_novel_time = get_predicate_indx(NOVEL_TIME)

    actions_from_plan = []

    for indx, (timestamp, action) in enumerate(plan_data, start=0):
        
        indx_action = get_action_from_lifted_name(action)

        indx_action, g_pre, g_eff = actions[indx_action]

        andstr, g_pre = g_pre
        andstr, g_eff = g_eff

        if schema == 'plain':
            time_equal = EQUAL, [indx_novel_time, str(timestamp)]
        else:
            time_equal = EQUAL, [TOTAL_TIME, str(timestamp)]
        
        done       = get_predicate_indx(EVENT_DONE.format(str(indx)))

        pre_action = AND, [done, time_equal] + g_pre

        done_prime = get_predicate_indx(EVENT_DONE.format(str(indx + 1)))
        eff_list = [(NOT, done), done_prime] + g_eff

        if val_schema in [Z2, Z3]:
            mono_done_indx = get_predicate_indx(MONO_EVENT_DONE.format(indx))
            eff_list.append(mono_done_indx)

        eff_action = AND, eff_list 

        event_name = SIMULATE_ACTION.format(action.replace(' ', '--'), str(indx))
        indx_event = update_grounded_action(event_name, [])
        # events.append((indx_event, pre_action, eff_action))
        actions_from_plan.append((indx_event, pre_action, eff_action))

    # return events
    return actions_from_plan

