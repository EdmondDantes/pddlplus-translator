import os
import nltk
import math
import click
import datetime
import matplotlib.pyplot as plt

from nltk.corpus import treebank
from networkx    import *
from os.path     import join, exists
from copy        import deepcopy

CURRENT_PATH = os.getcwd()

DOMAIN_KW       = 'domain'
TYPES_KW        = ':types'
PREDICATES_KW   = ':predicates'
FUNCTIONS_KW    = ':functions'
PROCESS_KW      = ':process'
PARAMETERS_KW   = ':parameters'
PRECONDITION_KW = ':precondition'
EFFECT_KW       = ':effect'
ACTION_KW       = ':action'
EVENTS_KW       = ':event'
PROCESSES_KW    = ':process'
INIT_KW         = ':init'
GOAL_KW         = ':goal'

METRIC = '(:metric minimize (total-time))\n'

# Domain strings
DOMAIN_NAME = 'domain_name'
TYPES       = 'types'
PREDICATES  = 'predicates'
FUNCTIONS   = 'functions'
PROCESSES   = 'processes'
EVENTS      = 'events'
ACTIONS     = 'actions'


# Global map strings
GROUNDED_VARIABLES  = 'grounded_variable'
GROUNDED_ACTIONS    = 'grounded_actions'
GROUNDED_PREDICATES = 'grounded_predicates'
GROUNDED_FUNCTIONS  = 'grounded_functions'
# Copyall map
DONE_LIST      = 'done_list'
COPY_INDX_DICT = 'copy_indx_dict'

G_A     = 'g_a'
G_E     = 'g_e'
G_P     = 'g_p'
G_PRED  = 'g_pred'
G_FUNCT = 'g_funct'

# Problem strings
PROBLEM_NAME = 'problem_name'
OBJECTS      = 'objects'
INIT_STATE   = 'init_state'
GOAL_STATE   = 'goal_state'

PARSED_EMPTY_PRECONDITION = '', []


WHEN      = 'when'
NOT       = 'not'
AND       = 'and'
OR        = 'or'
LESS      = '<'
LESSEQ    = '<='
GREATER   = '>'
GREATEREQ = '>='
EQUAL     = '='
ASSIGN    = 'assign'
INCREASE  = 'increase'
DECREASE  = 'decrease'
MINUS     = '-'
TIMES     = '*'
POWER     = '^'
DIVISION  = '/'
PLUS      = '+'
NAN       = None

NUMERIC_OPERATORS = [LESS, LESSEQ, GREATER, GREATEREQ, EQUAL, NOT, ASSIGN, DECREASE, INCREASE]
LOGICAL_OPERATORS = [AND, OR]
OPERANDS_SYMBOLS  = [MINUS, TIMES, POWER, DIVISION, PLUS]

PROPOSITIONAL = 'prop'
NUMERICAL     = 'num'

LVALUE = 0
RVALUE = 1

CONTINOUS_TIME = '#t'

FALSE   = False  # truth value
TRUE    = True
NEG     = False  # neg = del, pos = add
POS     = True
DISCARD = None  # used to discard grounded actions with pre(a) = False
 
NO_CASCADING_E  = False
YES_CASCADING_E = True

INVARIANT     = True
INVARIANTS    = 'invariant'
UNDEFINED_PAR = None
NO_PLAN       = None


NOT_FOUND = -1

''' new compiled predicates '''
UNDEFINED         = 'undefined'                   # invalid
FORCE_EVENTS      = 'force-events'                # invalid/copyall
FIRED             = 'fired--{}'                   # invalid/copyall
# DONE              = 'done--{}--{}'              # copyall
DONE              = 'done--{}'                    # copyall
COPY              = 'copy--{}'                    # copyall
PAUSE             = 'pause'                       # copyall
SIMULATE          = 'simulate--{}--{}'            # copyall
SIMULATE_POSITIVE = 'simulate--positive--{}--{}'  # copyall
SIMULATE_NEGATIVE = 'simulate--negative--{}--{}'  # copyall


''' new compiled actions '''
SIMULATE_EVENTS    = 'simulate-events'
SIMULATE_PROCESSES = 'simulate-processes'
START_SIMULATE     = 'start-simulate-processes'
END_SIMULATE       = 'end-simulate-processes'

''' new functions '''
DELTA        = 'delta'
MINUS_DELTA  = 'minus-delta'
TOTAL_TIMEs  = 'total-time'
TOTAL_TIME   = '(total-time)'
NOVEL_TIME   = 'novel-time'


''' compilation methods '''
INVALID     = 'invalid'     # POLY-minus
INVALIDplus = 'invalidplus' # POLY-ominus
COPYALL     = 'copyall'     # POLY
EXP         = 'exp'         # EXP
POLYBEST    = 'polybest'    # POLY*
COPYALLplus = 'copyallplus' # ad hoc POLY combined with Z1

''' correct compilation names '''
POLY      = 'POLY'
EXPbold   = 'EXP'
POLYMINUS = 'POLY-'
EXPLOCAL  = 'EXPL'


''' compilation methods for validation '''
Z0           = 'z0'
Z1           = 'z1'
Z2           = 'z2'
Z3           = 'z3'
Z0_PLAIN     = 'z0+plain'
Z0_COPYALL   = 'z0+copyall'
Z0_EXP       = 'z0+exp'
Z1_PLAIN     = 'z1+plain'
Z1_COPYALL   = 'z1+copyall'
Z1_EXP       = 'z1+exp'
Z2_COPYALL_2 = 'z2+copyallplus' # ad hoc version of Z1_COPYALL for POLY
Z2_PLAIN     = 'z2+plain'
Z2_COPYALL   = 'z2+copyall'
Z3_PLAIN     = 'z3+plain'
Z3_COPYALL   = 'z3+copyall'

''' paper names '''
PLAIN        = 'PLAIN'
Z0p          = 'V0'
Z1p          = 'VU'
Z2p          = 'VUD'
#
Z0_PLAINp    = '{}+{}'.format(Z0p, PLAIN)
Z0_POLYp     = '{}+{}'.format(Z0p, POLY)
# Z0_EXPbold = '{}+{}'.format(Z0p, EXPbold)
#
Z1_PLAINp    = '{}+{}'.format(Z1p, PLAIN)
Z1_POLYp     = '{}+{}'.format(Z1p, POLY)
# Z1_EXPbold = '{}+{}'.format(Z1p, EXPbold)
#
Z2_PLAINp    = '{}+{}'.format(Z2p, PLAIN)
Z2_POLYp     = '{}+{}'.format(Z2p, POLY)
Z2_POLYVp    = 'POLYV'
# Z2_EXPbold = '{}+{}'.format(Z2p, EXPbold)

ALLOWED_SCHEME = [
    COPYALL, EXP, INVALID, INVALIDplus]
ALLOWED_SCHEME_EXTERNAL = [
    POLY, EXPbold, POLYMINUS, EXPLOCAL]

COMPOSED_SCHEME = [
    Z0_PLAIN, Z0_COPYALL, 
    Z1_PLAIN, Z1_COPYALL,
    Z2_PLAIN, Z2_COPYALL, Z2_COPYALL_2]
    # Z3_PLAIN, Z3_COPYALL]

# ALLOWED_SCHEME = [
#     COPYALL, EXP, INVALID, INVALIDplus, 
#     POLY, EXPbold, POLYMINUS, EXPLOCAL,
#     Z0_PLAIN, Z0_COPYALL, Z0_EXP, Z1_PLAIN, Z1_COPYALL, Z1_EXP, Z2_COPYALL_2, Z2_PLAIN, Z2_COPYALL]


''' Validation translation '''
TRANSL_per_VAL   = True
TRANSLATION      = False
SIMULATE_TIME    = 'simulate_novel_time'
PROP_TRUE        = 'prop-true'
PROP_FALSE       = 'prop-false'
MONO_EVENT_DONE  = 'mono-event-done--{}'         # Used by Z1
EVENT_DONE       = 'event-done--{}'              # Used by Z0, Z1
SIMULATE_ACTION  = 'simulate_action-{}-{}'
START_SIMULATE_t = 'start-simulate-processes-{}'
TIMED_EVENT      = 'TIMED_EVENTS_{}'             # Used by Z1
BOUND_EVENT      = 'time_bound_event'


# TEMPORARY [TROVARE DEFINIZIONE DI CASCADING EVENTS]
baxter_cascading = None

'''
Configuration 
'''
USE_CONDEFF_PROC = False


def readfile(path):
    with open(path, 'r') as fin:
        return fin.read()


def writefile(path, string):
    # if exists(path):
    if False:
        print ('The {} path is not free'.format(path))
    else:
        with open(path, 'w') as fout:
            fout.write(string)


def write_temporary_file(string):
    temp_name = 'temp_file_{}'.format(str(datetime.datetime.now()).replace(' ', '-'))
    temp_name = join(CURRENT_PATH, temp_name)
    with open(temp_name, 'w') as fout:
        fout.write(string)
    return temp_name


def merge_list_of_lists(lists):
    new_list = []
    for element in lists:
        for t in element:
            new_list.append(t)
    return new_list


def not_variable(value):
    return value in LOGICAL_OPERATORS + NUMERIC_OPERATORS + OPERANDS_SYMBOLS


def extract_leaves(expr, leaves_set):
    '''
    This is a recursive function that, given a tree expression 'expr',
    populates the set 'leaves_set'.
    '''
    if type(expr) == int:    # variable indx
        leaves_set.add(expr)
    elif type(expr) == str:  # #t or costant
        pass
    elif type(expr) == tuple and len(expr) == 2:
        lvalue = expr[LVALUE]
        if lvalue in [ASSIGN, DECREASE, INCREASE]:
            extract_leaves (expr[RVALUE][LVALUE], leaves_set)
        if lvalue in [NOT]:
            # print (expr[RVALUE].__class__.__name__)
            extract_leaves (expr[RVALUE], leaves_set)
        else: 
            #type(expr) == tuple:
            rvalue = expr[RVALUE]
            [extract_leaves(el, leaves_set) for el in rvalue]
    elif type(expr) == list:
        [extract_leaves(el, leaves_set) for el in expr]
    else:
        print ('ERROR in extract_leaves')
        print (type(expr), expr)
        assert False


def extract_leaves_full(expr, leaves_set):
    '''
    This is a recursive function that, given a tree expression 'expr',
    populates the set 'leaves_set'.
    '''
    if type(expr) == int:    # variable indx
        leaves_set.add(expr)
    elif type(expr) == str:  # #t or costant
        pass
    elif type(expr) == tuple and len(expr) == 2:
        lvalue = expr[LVALUE]
        if lvalue in [ASSIGN, DECREASE, INCREASE]:
            extract_leaves_full (expr[RVALUE][RVALUE], leaves_set)
            extract_leaves_full (expr[RVALUE][LVALUE], leaves_set)
        elif lvalue == NOT:
            extract_leaves_full (expr[RVALUE], leaves_set)
        else: 
            #type(expr) == tuple:
            rvalue = expr[RVALUE]
            [extract_leaves_full(el, leaves_set) for el in rvalue]
    elif type(expr) == list:
        [extract_leaves_full(el, leaves_set) for el in expr]
    else:
        print (expr)
        print (type(expr))
        assert False



def extract_lifted_leaves(expr, leaves_set):
    if type(expr) == str:  # #t or costant
        leaves_set.add(expr)
    elif type(expr) == list:
        [extract_lifted_leaves(el, leaves_set) for el in expr]
    elif type(expr) == tuple and len(expr) == 2:
        rvalue = expr[LVALUE]
        lvalue = expr[RVALUE]
        extract_lifted_leaves(rvalue, leaves_set)
        extract_lifted_leaves(lvalue, leaves_set)
    else:
        assert False


def is_tree(t):
    return type(t) == nltk.Tree


def get_last_name(path):
    return path.split('/')[-1]


def is_number(string):
    string = string.replace('.', '').replace('-', '')
    return string.isnumeric()


# Function to generate all binary strings  
# from: https://gist.github.com/kevinwuhoo/2424597
def generate_binary(n):

    # 2^(n-1)  2^n - 1 inclusive
    bin_arr = range(0, int(math.pow(2,n)))
    bin_arr = [bin(i)[2:] for i in bin_arr]

    # Prepending 0's to binary strings
    max_len = len(max(bin_arr, key=len))
    bin_arr = [i.zfill(max_len) for i in bin_arr]

    return bin_arr


def set_baxter_cascading(domain_str):
    global baxter_cascading
    # baxter_cascading = '(domain paco3d)' in domain_str or '(domain car_linear_mt_sc)' in domain_str
    baxter_cascading = '(define (domain urbantraffic)' in domain_str



def get_baxter_cascading():
    global baxter_cascading
    return baxter_cascading


def print_header(string):
    print(len(string)*'-' + '\n' + string + '\n' + len(string)*'-')