from utils      import *
from global_map import * 

from itertools import product 


def ground_domain(domain_dict, problem_dict, g_problem_init):

    print ('Grounding domain [Naive]')

    objects    = problem_dict[OBJECTS]
    predicates = domain_dict[PREDICATES]
    functions  = domain_dict[FUNCTIONS]
    variables  = {PREDICATES: [name for (name, _) in predicates], FUNCTIONS: [name for (name, _) in functions]}

    print ('\tGrounding actions')
    grounded_A = ground_actions(domain_dict[ACTIONS], objects, domain_dict[INVARIANTS], variables, g_problem_init)
    print ('\t\t|g_A| = {}'.format(len(grounded_A)))

    print ('\tGrounding events')
    grounded_E = ground_actions(domain_dict[EVENTS], objects, domain_dict[INVARIANTS], variables, g_problem_init)
    print ('\t\t|g_E| = {}'.format(len(grounded_E)))

    print ('\tGrounding processes')
    grounded_P = ground_actions(domain_dict[PROCESSES], objects, domain_dict[INVARIANTS], variables, g_problem_init)
    print ('\t\t|g_P| = {}'.format(len(grounded_P)))

    grounded_predicates, grounded_functions = get_list_of_grounded_preds_and_functions()
    
    grounded_domain_dict = {
        DOMAIN_NAME : 'grounded-' + domain_dict[DOMAIN_NAME],
        G_A         : grounded_A,
        G_E         : grounded_E,
        G_P         : grounded_P,
        G_PRED      : grounded_predicates,
        G_FUNCT     : grounded_functions
    }

    print ('\n')
    return grounded_domain_dict


def ground_actions(lifted_actions, objects, invariants, variables, g_problem_init):
    grounded_actions = []
    for (name, pars, pre, eff) in lifted_actions:
        objects_lists = get_sets2combinate(pars, objects)
        for combination in product(*objects_lists):
            if not discard_combination(combination):
                g_action = ground_single_action(name, pars, pre, eff, combination, variables, invariants, g_problem_init)
                if g_action is not DISCARD:
                    grounded_actions.append(g_action)
    return grounded_actions


def get_sets2combinate(pars, objects):
    '''
    Function that generates the set of objects
    of which cartesian product is to be made
    '''
    a_typ = [[typ for i in  range(0, len(list_of_par))] for list_of_par, typ in pars]
    a_typ = merge_list_of_lists(a_typ)
    objects_lists = [objects[typ] for typ in a_typ]
    return objects_lists


def discard_combination(combination):
    return len(set(combination)) < len(combination)


def ground_formula(formula, pars, substituion, variables, invariants, g_problem_init):
    # substitution
    g_formula = recursive_ground_formula(formula, substituion, variables, invariants, g_problem_init)

    # inference [fino a che la formula non satura => semplifico]
    while True:
        simplified_g_formula = simplify_formula(g_formula)
        if (simplified_g_formula) == (g_formula):
            break
        g_formula = simplified_g_formula

    # patch [NOT MORE NECESSARY?]
    if type(simplified_g_formula) == tuple:
        if simplified_g_formula[LVALUE] == AND:
            if False in simplified_g_formula[RVALUE]:
                return False

    return simplified_g_formula


def simplify_formula(g_formula):
    '''
    Function which simplifies AND and OR with TRUE or FALSE
    '''

    if type(g_formula) == tuple:
        assert len(g_formula) == 2
        lvalue = g_formula[LVALUE]
        rvalue = g_formula[RVALUE]
        if lvalue == AND:

            if TRUE in rvalue:
                return AND, [simplify_formula(sub_rvalue) for sub_rvalue in rvalue if sub_rvalue is not TRUE]
            elif FALSE in rvalue:
                return FALSE

        elif lvalue == OR:
            if TRUE in rvalue:
                return TRUE
            elif FALSE in rvalue:
                return OR, [simplify_formula(sub_rvalue) for sub_rvalue in rvalue if sub_rvalue is not FALSE]

        elif lvalue == NOT and rvalue in [TRUE, FALSE]:
            print ('SISTEMA simplify_formula')
            exit(1)

        return lvalue, [simplify_formula(sub_rvalue) for sub_rvalue in rvalue]
    else:
        return g_formula


def recursive_ground_formula(formula, substituion, variables, invariants, g_problem_init):
    if formula == PARSED_EMPTY_PRECONDITION:
        return PARSED_EMPTY_PRECONDITION

    elif formula == []:  # il predicato non ha parametri
        return ''
    elif type(formula) == str:
        return formula
    else:
        lvalue = formula[LVALUE]
        rvalue = formula[RVALUE]
        if not_variable(lvalue):
            return lvalue, [recursive_ground_formula(rvalue_indx, substituion, variables, invariants, g_problem_init) for rvalue_indx in rvalue]
        else:
            if lvalue in invariants:
                if lvalue in variables[FUNCTIONS]:
                    # return ground_variable(lvalue, rvalue, substituion, variables)
                    return fetch_numeric_value_from_I(lvalue, rvalue, substituion, g_problem_init)
                elif lvalue in variables[PREDICATES]:
                    return fetch_truth_value_from_I(lvalue, rvalue, substituion, variables, g_problem_init)
            else:
                return ground_variable(lvalue, rvalue, substituion, variables)


def fetch_numeric_value_from_I(variable_name, lifted_pars, substituion, g_problem_init):
    g_p_prop = [x for x in g_problem_init if type(x) != int]
    grounded_fluent = ground_str_var(variable_name, substituion, lifted_pars)
    indx_grounded   = get_predicate_indx(grounded_fluent)
    for lvalue, rvalue in g_p_prop:
        if lvalue == NOT:      # e.g. in DinoCar there is a (not (pred)) stated in the initial state; we ignore it [refactor]
            continue
        var   = rvalue[LVALUE]
        value = rvalue[RVALUE]
        if var == indx_grounded:
            return value
    # print ('Undefined value for invariant variable: {}'.format(grounded_fluent))
    return None


def fetch_truth_value_from_I(variable_name, lifted_pars, substituion, variables, g_problem_init):
    grounded_fluent = ground_str_var(variable_name, substituion, lifted_pars)
    indx_grounded   = get_predicate_indx(grounded_fluent)
    if indx_grounded == NOT_FOUND:
        return FALSE
    else:
        return find_if_I_imply_g_var(indx_grounded, g_problem_init)


def find_if_I_imply_g_var(g_v_indx, g_problem_init):
    '''
    Just for propositional variables
    '''
    g_p_prop = [x for x in g_problem_init if type(x) == int]
    return g_v_indx in g_p_prop


def ground_variable(variable_name, lifted_pars, substituion, variables):
    # print ('DEBUG')
    # print ('variable_name: {}'.format(variable_name))
    # print ('lifted_pars  : {}'.format(lifted_pars))
    # print ('substituion  : {}'.format(substituion))

    grounded_fluent = ground_str_var(variable_name, substituion, lifted_pars)
    return update_grounded_predicates(grounded_fluent, variable_name, variables)


def ground_str_var(variable_name, substituion,lifted_pars):
    obj = ['__{}'.format(substituion[lifted_par]) for lifted_par in lifted_pars]
    grounded_fluent = variable_name + ''.join(obj)
    return grounded_fluent


def ground_single_action(name, pars, pre, eff, combination, variables, invariants, g_problem_init):
    substituion = get_substituion(pars, combination)
    g_pre = ground_formula(pre, pars, substituion, variables, invariants, g_problem_init)
    if g_pre is FALSE:
        return DISCARD
    g_eff = recursive_ground_formula(eff, substituion, variables, invariants, g_problem_init)
    indx_action = update_grounded_action(name, combination)
    
    return (indx_action, g_pre, g_eff)


def get_substituion(pars, combination):
    variables_dict = {}
    indx = 0
    for p_vars, v_typ in pars:
        for p_var in p_vars:
            variables_dict[p_var] = combination[indx]
            indx += 1
    return variables_dict


'''
GROUND PROBLEM
'''
def ground_problem(domain_dict, problem_dict):
    print ('Grounding problem')

    init_state = problem_dict[INIT_STATE]
    goal_state = problem_dict[GOAL_STATE]

    # refact: e' identico a quello in ground_domain
    predicates = domain_dict[PREDICATES]
    functions  = domain_dict[FUNCTIONS]
    variables = {PREDICATES: [name for (name, _) in predicates], FUNCTIONS: [name for (name, _) in functions]}
    # end-refactor
    
    # cambiare nome - controintuitivo

    g_init_state = ground_initial_state(init_state, variables)

    g_goal_state = ground_initial_state(goal_state, variables)

    grounded_problem_dict = {
        PROBLEM_NAME : 'grounded-' + problem_dict[PROBLEM_NAME],
        INIT_STATE   : g_init_state,
        OBJECTS      : [],
        INIT_STATE   : g_init_state,
        GOAL_STATE   : g_goal_state
    }

    return grounded_problem_dict


def ground_initial_state(init_state, variables):
    g_init_state = []
    if type(init_state) == list:
        return [ground_initial_state(p, variables) for p in init_state]
    else:
        if type(init_state) == str:
            '''
            Why this condition?
            Because, e.g. in DinoCar, there are, in the initial state
            assignment like (= a 10) which are parsed differently with nltk.
            Therefore this if manage the aforementioned case. 

            Other way: preprocessing. Each variable 'a' -> '(a)'
            '''
            if is_number(init_state):
                return init_state
            else:
                n = update_grounded_predicates(init_state, init_state, variables)
                return n 
        else:
            lvalue = init_state[0]
            rvalue = init_state[1]
            if not_variable(lvalue):
                return (lvalue, [ground_initial_state(p, variables) for p in rvalue])
            else:
                # ('running_time', [])
                # concateno semplicemente i parametri
                name = lvalue
                if len(rvalue) > 0:
                    name += '__' + '__'.join(rvalue)
                n = update_grounded_predicates(name, lvalue, variables)
                return n

