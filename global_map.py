from utils    import *

from bidict import bidict
from bidict import ValueDuplicationError

GLOBAL_MAP = {
    TYPES              : bidict({}),
    GROUNDED_VARIABLES : bidict({}),
    GROUNDED_ACTIONS   : bidict({}),
    GROUNDED_PREDICATES: set(),
    GROUNDED_FUNCTIONS : set(),
    #PREDS_DICT         : dict({})   # struttura che memorizza per ogni predicato gli indici delle variabili groundizzate
}


def update_grounded_predicates(grounded_name, variable_name, variables):

    if grounded_name == '':
        print ('ERROR in \'update_grounded_predicates\'')
        exit(1)
    n = get_predicates_size()
    if not exists_predicate(grounded_name):
        GLOBAL_MAP[GROUNDED_VARIABLES][n] = grounded_name
        update_pred_or_funct(variable_name, variables, n)
        return n
    else:
        return GLOBAL_MAP[GROUNDED_VARIABLES].inverse[grounded_name]


def generate_new_predicate(grounded_name):
    n = get_predicates_size()
    #try:
    GLOBAL_MAP[GROUNDED_VARIABLES][n] = grounded_name
    #except ValueDuplicationError:
    return GLOBAL_MAP[GROUNDED_VARIABLES].inverse[grounded_name]
    # return n


def generate_new_function(grounded_name):
    n = get_predicates_size()
    #try:
    GLOBAL_MAP[GROUNDED_VARIABLES][n] = grounded_name
    # except ValueDuplicationError:
    # return GLOBAL_MAP[GROUNDED_VARIABLES].inverse[grounded_name]     
    # GLOBAL_MAP[GROUNDED_FUNCTIONS] = n
    return n


def is_function(indx):
    return indx in GLOBAL_MAP[GROUNDED_FUNCTIONS]


def update_pred_or_funct(variable_name, variables, n):
    if variable_name in variables[PREDICATES]:
        k = GROUNDED_PREDICATES
    else:
        k = GROUNDED_FUNCTIONS
    GLOBAL_MAP[k].add(n)


def get_action_indx(grounded_name):
    return GLOBAL_MAP[GROUNDED_VARIABLES].inverse[grounded_name]


def exists_predicate(grounded_name):
    return grounded_name in GLOBAL_MAP[GROUNDED_VARIABLES].inverse.keys()


def exists_actions(grounded_name):
    return grounded_name in GLOBAL_MAP[GROUNDED_ACTIONS].inverse.keys()


def get_predicates_size():
    return len(GLOBAL_MAP[GROUNDED_VARIABLES])


def get_actions_size():
    return len(GLOBAL_MAP[GROUNDED_ACTIONS])


def get_types_size():
    return len(GLOBAL_MAP[TYPES])


def update_grounded_action(name, combination_of_objs):
    grounded_name = name
    objs = '__' + '__'.join(combination_of_objs) if len(combination_of_objs) > 0 else ''
    grounded_name += objs
    
    if not exists_actions(grounded_name):
        n = get_actions_size()
        GLOBAL_MAP[GROUNDED_ACTIONS][n] = grounded_name
        return n
    else:
        return GLOBAL_MAP[GROUNDED_ACTIONS].inverse[grounded_name]

# def get_action_name(indx):
#     return GLOBAL_MAP[GROUNDED_ACTIONS][indx]


def get_predicate_name(indx):
    if indx not in GLOBAL_MAP[GROUNDED_VARIABLES].keys():
        print ('Indx: {} not found'.format(indx))
        return NOT_FOUND
    else:
        return GLOBAL_MAP[GROUNDED_VARIABLES][indx]


def get_predicate_indx(name):
    if name not in GLOBAL_MAP[GROUNDED_VARIABLES].inverse.keys():
        x = NOT_FOUND
    else:
        x = GLOBAL_MAP[GROUNDED_VARIABLES].inverse[name]
    return x


def get_something_name(indx, token):
    # actions, events and processes
    return GLOBAL_MAP[GROUNDED_ACTIONS][indx]


def get_list_of_grounded_preds_and_functions():
    return GLOBAL_MAP[GROUNDED_PREDICATES], GLOBAL_MAP[GROUNDED_FUNCTIONS]


COPYALL_MAP = {
    DONE_LIST      : {},
    COPY_INDX_DICT : {},
}


def add_done_pred_global(indx, done_pred):
    COPYALL_MAP[DONE_LIST][indx] = done_pred


def get_list_of_done_preds():
    return list(COPYALL_MAP[DONE_LIST].values())


def update_copy_indx_dict(f_indx, copy_indx):
    COPYALL_MAP[COPY_INDX_DICT][f_indx] = copy_indx


def get_copy_variable_from_indx(f_indx):
    return COPYALL_MAP[COPY_INDX_DICT][f_indx]


def get_done_pred_indx(indx):
    return COPYALL_MAP[DONE_LIST][indx]


def get_last_done_pred_indx():
    return COPYALL_MAP[DONE_LIST][len(COPYALL_MAP[DONE_LIST]) - 1]


def get_first_done_pred_indx():
    return get_done_pred_indx(0)


def get_action_from_lifted_name(action):
    grounded = action.replace(' ', '__')

    return (GLOBAL_MAP[GROUNDED_ACTIONS].inverse[grounded])


def get_timed_actions(plan_data, makespan):
    seen_timestamp = [0.0]
    grouped_action = [[]]

    for indx, (time, action) in enumerate(plan_data):

        if time not in seen_timestamp:
            seen_timestamp.append(time)
            grouped_action.append([])

        indx_t = seen_timestamp.index(time)

        indx_action = get_action_from_lifted_name(action)

        grouped_action[indx_t].append(indx)

    if makespan not in seen_timestamp:
        seen_timestamp.append(makespan)
        grouped_action.append([])

    return grouped_action, seen_timestamp