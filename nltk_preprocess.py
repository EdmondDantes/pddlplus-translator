from utils import *

import re

PARS_REGEX = r':parameters[^()]*\('


def nltk_domain_preprepocess(domain_str):

    domain_str = preprocess_parameters(domain_str)

    domain_str = preprocess_colon(domain_str, ':precondition')

    domain_str = preprocess_colon(domain_str, ':effect')

    domain_str = uncomment(domain_str)

    domain_str = remove_minus(domain_str)

    return domain_str


def uncomment(domain_str):
    '''
    This function remove the comments
    '''
    comments_str = [domain_str[match.start():match.end()] for match in re.finditer(';[^\n]*\n', domain_str)]
    for c in comments_str:
        domain_str = domain_str.replace(c, '')
    return domain_str


def remove_minus(domain_str):
    return domain_str


def preprocess_parameters(domain_str):
    pars = set((re.findall(PARS_REGEX, domain_str)))

    for p in pars:
        assert p in domain_str
        domain_str = domain_str.replace(p, '(:parameters ')

    return domain_str


def get_first_indx_bracket(indx, domain_str):
    while True:
        if domain_str[indx] == '(':
            return indx
        indx += 1


def preprocess_colon(domain_str, token):
    '''
    Function which preprocess precondition (effect) definition
    in order to exploit NLTK. Example:
        Input : :precondition (engine_stopped)
        Output: (:precondition (engine_stopped))
    '''
    x = re.finditer(token, domain_str)
    prec_list = []
    for match in x:
        start = match.start()
        indx = get_first_indx_bracket(start, domain_str)
        prec_expr = domain_str[start:indx] + close_brackets(indx, domain_str)
        prec_list.append(prec_expr)
    
    # Setify in order to avoid repetition and then error
    prec_list = set(prec_list)

    for prec_expr in prec_list:
        domain_str = domain_str.replace(prec_expr, prec_expr.replace(':', '(:') + ')')

    return domain_str
    # print (domain_str) 
    # exit(1)

    # new_domain_str = domain_str
    # precs = re.finditer(regex_token, domain_str)
    # for p in precs:
    #     preconditions_str = close_brackets_iter(p, domain_str)
    #     new_preconditions_str = preconditions_str.replace(token, token.replace(':', '(:')) + ')'
    #     assert preconditions_str in new_domain_str
    #     new_domain_str = new_domain_str.replace(preconditions_str, new_preconditions_str)
    # return new_domain_str


# def close_brackets_iter(p, domain_str):
#     start  = p.end()
#     indx = start
#     b = 1
#     while True:
#         if domain_str[indx] == '(':
#             b += 1
#         elif domain_str[indx] == ')':
#             b += -1
#         if b == 0:
#             break
#         indx += 1
#     return domain_str[p.start():indx + 1]


def close_brackets(start, domain_str):
    indx  = start + 1
    b = 1
    while True:
        if domain_str[indx] == '(':
            b += 1
        elif domain_str[indx] == ')':
            b += -1
        if b == 0: break
        indx += 1
    return domain_str[start:indx + 1]


def nltk_problem_preprepocess(problem_str):

    problem_str = uncomment(problem_str)

    return problem_str  