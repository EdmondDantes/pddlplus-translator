import re

TI_R = '(= (time_requested r1 k{}) {})'
TE_R = '(= (temp_requested r1 k{}) {})'


REPLACE = '(= (time) 0)'

def main():
    base16 = read_instance16('HVAC-long/instance_1_16.pddl')

    for ik in range(17, 101):
        new_problem = generate_iii_instance(ik, base16)

        write_instance(new_problem, 'HVAC-long/instance_1_{}.pddl'.format(ik))


def write_instance(new_problem, path):
    with open(path, 'w') as fout:
        fout.write(new_problem)


def read_instance16(path):
    with open(path, 'r') as fout:
        return fout.read()


def generate_iii_instance(ik, base16):
    new_base16 = base16

    ''' Objects '''
    objs_str   = get_objs(base16)
    new_objstr = objs_str
    new_objstr   = objs_str.replace(' -request)', '')

    for iik in range(17, ik + 1):
        new_objstr += ' k{}'.format(str(iik))
    new_objstr += ' -request)'
    new_base16 = new_base16.replace(objs_str, new_objstr)

    ''' Init '''
    init_str = ''
    for iik in range(17, ik + 1):
        if iik % 2 == 1:
            value_str = '14'
        elif iik % 2 == 0:
            value_str = '20'

        init_str += '\t\t' + TI_R.format(str(iik), str(iik * 10)) + '\n'
        init_str += '\t\t' + TE_R.format(str(iik), value_str) + '\n'
    new_base16 = new_base16.replace(REPLACE, '(= (time) 0)\n{}'.format(init_str))


    ''' goal '''
    goal = ''
    for iik in range(17, ik + 1):
        goal += '              (satisfied k{})\n'.format(str(iik))
    new_base16 = new_base16.replace('(satisfied k16)', '(satisfied k16)\n' + goal)

    return new_base16


def get_objs(string):
    objs_list = (re.findall(r'\(:objects[^\)]+\)', string))
    assert len(objs_list) == 1
    obj_str = objs_list[0]
    return obj_str


if __name__ == "__main__":
    main()