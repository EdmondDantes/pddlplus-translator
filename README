***********
Description
***********
A compiler that translates PDDL+ problems into numeric problems as described in [1]. The compiler supports both polynomial (POLY) and exponential (EXP) schemes. The tool also supports the events compilation (possibly in cascade).

**********
How to use
**********

	****************
	Compilation mode
	****************
	python3.6 pddlplus_compiler.py --domain_path DOMAIN_PATH --problem_path PROBLEM_PATH --output_path OUTPUT_PATH --schema SCHEMA --delta DELTA --no_event_opt_cascade NO-EVENT-OPT-CASCADE --no_event_opt_action NO-EVENT-OPT-ACTION where:
		- DOMAIN_PATH, PROBLEM_PATH, OUTPUT_PATH are self-explainatory;
		- SCHEMA is the a translation schema between {EXP, POLY, POLY-, EXPL}, where:
			- POLY and EXP are the translations presented in [1];
			- POLY- is the sound but incomplete variant of POLY presented in [2][3];
			- EXPL is an optimised variant of EXP (L stands for Local) presented in [4];
		- DELTA is a float represeting the time discretization time step;
		- NO-EVENT-OPT-CASCADE/NO-EVENT-OPT-ACTION assume the value {True, False}. The Boolean parameters --no_event_opt_cascade and --no_event_opt_action are used to disable some optimisation of the used schema. The standard configuration sets these two parameters as False, so the optimisations are enabled. The optimisation are not yet documented. 
		- The compiler assumes OUTPUT_PATH be an existing directory

	***************
	Validation mode
	***************	
	python3.6 pddlplus_compiler.py --domain_path DOMAIN_PATH --problem_path PROBLEM_PATH --output_path OUTPUT_PATH --schema SCHEMA --delta DELTA --no_event_opt_cascade NO-EVENT-OPT-CASCADE --no_event_opt_action NO-EVENT-OPT-ACTION --plan PLAN_PATH where all the parameters, except SCHEMA and PLAN_PATH are defined as before and:
	- PLAN_PATH is the path of the plan to be validated through the reformulation ([5])
	- SCHEMA is a translation for doing validation between {V0+PLAIN, V0+POLY, VU+PLAIN, VU+POLY, VUD+PLAIN, VUD+POLY, POLYV}. All these schemata are documented in [5]. Note that {V0+PLAIN, VU+PLAIN, VUD+PLAIN} are PDDL+ to PDDL+ translations while {V0+POLY, VU+POLY, VUD+POLY, POLYV} are PDDL+ to PDDL2.1 translations. The discretisation parameter DELTA can be used just for the PDDL+ to PDDL2.1 translations.


Required Libraries:
	- nltk
	- click
	- datetime
	- bidict
	- sympy
All libraries can be installed using pip3


[1] Francesco Percassi, Enrico Scala, Mauro Vallati. "Translations from Discretised PDDL+ to Numeric Planning", In Proceedings of the 31st International Conference on Automated Planning and Scheduling (ICAPS 2021), 2021.

[2] Francesco Percassi, Enrico Scala, Mauro Vallati. "A Sound (But Incomplete) Polynomial Translation from Discretised PDDL+ to Numeric Planning", In Proceedings of The Knowledge Engineering for Planning and Scheduling Workshop (KEPS 2021), 2021.

[3] Francesco Percassi, Enrico Scala, Mauro Vallati. "A Sound (But Incomplete) Polynomial Translation from Discretised PDDL+ to Numeric Planning." International Conference of the Italian Association for Artificial Intelligence (AIxIA 2021), 2021.

[4] Francesco Percassi, Enrico Scala, Mauro Vallati. “On Translation-Based Approaches from Discrete PDDL+ to Numeric Planning.” In Proceedings of Knowledge Engineering for Planning and Scheduling (KEPS 2022), 2022.

[5] Francesco Percassi, Enrico Scala, Mauro Vallati. The Power of Reformulation: From Validation to Planning in PDDL+. In Proceedings of the 32nd International Conference on Automated Planning and Scheduling (ICAPS 2022), 2022.