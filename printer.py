from utils      import *
from global_map import *


def print_grounded_domain(g_domain_dict):

    print ('Generating grounded domain\n')

    domain_str = '(define (domain {})\n'.format(g_domain_dict[DOMAIN_NAME])
    
    domain_str += print_grounded_variables(g_domain_dict[G_PRED], PREDICATES_KW)
    domain_str += print_grounded_variables(g_domain_dict[G_FUNCT], FUNCTIONS_KW)

    domain_str += print_grounded_something(g_domain_dict[G_A], ACTION_KW)
    domain_str += print_grounded_something(g_domain_dict[G_E], EVENTS_KW)
    domain_str += print_grounded_something(g_domain_dict[G_P], PROCESSES_KW)
    
    domain_str += ')'

    return domain_str


def print_grounded_variables(variables, token):
    toreturn = '({}\n'.format(token)
    for p in variables:
        # print ('DEBUG: {}, {}'.format(p, get_predicate_name(p)))
        toreturn += '\t({})\n'.format(get_predicate_name(p))
    toreturn += ')\n\n'
    return toreturn


def print_grounded_something(g_actions_list, token):
    depth = 1
    toreturn = ''
    for something_tup in g_actions_list:
        index_something, g_pre, g_eff = something_tup
        something_str  = '({} {}\n'.format(token, get_something_name(index_something, token))
        something_str += ' :parameters()\n'

        # precondition
        if not is_empty_condition(g_pre):
            something_str += ' :precondition ' + print_grounded_formula(g_pre, depth) + '\n'
        something_str += ' :effect ' + print_grounded_formula(g_eff, depth) + '\n'

        something_str += ')\n\n'

        toreturn += something_str
    return toreturn


def is_empty_condition(g_pre):
    lvalue, rvalue = g_pre[0], g_pre[1]
    return rvalue == PARSED_EMPTY_PRECONDITION


def print_grounded_formula(g_formula, depth):
    if type(g_formula) == tuple:
        lvalue = g_formula[0]
        rvalue = g_formula[1]
        printed_str = '({} {})'.format(lvalue, print_grounded_formula(rvalue, depth + 1))
        
        prefix = '\n' + '\t' * depth

        return prefix + printed_str
    elif type(g_formula) == list:
        return '\n' + '\t' * depth + ''.join([print_grounded_formula(element, depth + 1) + ' ' for element in g_formula])
    elif type(g_formula) == int:
        return '({})'.format(get_predicate_name(g_formula))
    elif type(g_formula) == str:
        return g_formula
    else:
        print ('CASO NON GESTITO')
        print (g_formula)
        exit(1)


def print_grounded_problem(g_problem_dict, g_domain_dict):

    domain_str  = '(define (problem {})\n'.format(g_problem_dict[PROBLEM_NAME])
    domain_str += '(:domain {})\n'.format(g_domain_dict[DOMAIN_NAME])
    domain_str += print_grounded_init_state(g_problem_dict[INIT_STATE])
    
   
    domain_str += print_grounded_goal_state(g_problem_dict[GOAL_STATE])

    domain_str += METRIC

    domain_str += ')'
    return domain_str


def print_grounded_init_state(init_state):
    init_str  = '({}\n'.format(INIT_KW)
    init_str += '\t{}\n'.format(print_grounded_formula(init_state, 1))
    init_str += ')\n'

    return init_str


def print_grounded_goal_state(goal_state):
    goal_str  = '({}\n'.format(GOAL_KW)
    goal_str += '\t{}\n'.format(print_grounded_formula(goal_state, 1))
    goal_str += ')\n'

    return goal_str